---
title: About me
---

Пытаюсь читать, изучать:

* [Командная строка Linux. Полное руководство](https://www.litres.ru/uilyam-shotts/komandnaya-stroka-linux-polnoe-rukovodstvo-42227099/) (rus)
* [Использование Docker](https://www.litres.ru/edrien-mouet/ispolzovanie-docker-44336399/) (rus)
* [Проект «Феникс»](https://www.litres.ru/dzhin-kim/proekt-feniks-roman-o-tom-kak-devops-menyaet-biznes-k-l-54172333/) (rus)
* [Kubernetes The Hard Way](https://github.com/kelseyhightower/kubernetes-the-hard-way) (eng)
* В процессе: [Nginx Cookbook](https://www.nginx.com/wp-content/uploads/2017/07/Complete-NGINX-Cookbook-2019.pdf) (eng)
<!--
* [The little redis book](https://github.com/akandratovich/the-little-redis-book/blob/master/ru/redis.md) (rus)
-->


Полезное:

* [git](https://learngitbranching.js.org/?locale=ru_RU)
* [sql](https://sqlbolt.com/lesson/select_queries_introduction)
* [yaml](https://yaml-multiline.info/)
