---
author: "Sergey Chulanov"
title: "Run docker image for arm64 on amd64"
date: 2025-03-04T22:16:22+03:00
description: >-
  Run docker image for arm64 on amd64
tags:
  - docker
categories:
  - linux
series:
  - guide
draft: false
---

Display the host architecture
```bash
uname -m
x86_64
```

Let's try run image with `arm64`. We've got an error
```bash
docker run --rm -t arm64v8/ubuntu uname -m
WARNING: The requested image's platform (linux/arm64/v8) does not match the detected host platform (linux/amd64/v3) and no specific platform was requested 
exec /usr/bin/uname: exec format error
```

Install the qemu packages
```bash
sudo apt-get install qemu binfmt-support qemu-user-static
```

This step will execute the registering scripts
```bash
docker run --rm --privileged multiarch/qemu-user-static --reset -p yes
```

Check
```bash
cat /proc/sys/fs/binfmt_misc/qemu-aarch64
enabled
interpreter /usr/bin/qemu-aarch64-static
flags: F
offset 0
magic 7f454c460201010000000000000000000200b700
mask ffffffffffffff00fffffffffffffffffeffffff

ls /usr/bin/qemu-aarch64*
/usr/bin/qemu-aarch64_be-static  /usr/bin/qemu-aarch64-static
```

Testing the emulation environment
```bash
docker run --platform linux/arm64 --rm -t arm64v8/ubuntu uname -m
aarch64
```

Useful links:
- https://github.com/multiarch/qemu-user-static
- https://hub.docker.com/r/multiarch/qemu-user-static/
