---
author: "Sergey Chulanov"
title: "Java(war) + tomcat + nexus"
date: 2023-02-07T22:35:08+03:00
description: >-
  Hello world on java(war) + tomcat + nexus
tags:
  - java
  - tomcat
  - nexus
categories:
  - devops
series:
  - guide
draft: false
---

> Quick start with Spring Boot [here](https://spring.io/guides/gs/spring-boot/)

Wep app (war):
```bash
# Install maven and export bin
export PATH=$PATH:/opt/maven/apache-maven-3.8.7/bin

MAVEN_APP_TYPE=maven-archetype-webapp \
  && APP_GROUP=app.awesome.my \
  && APP_NAME=myawesomeapp

mvn archetype:generate \
        -DgroupId="${APP_GROUP}" \
        -DartifactId="${APP_NAME}" \
        -DarchetypeArtifactId="${MAVEN_APP_TYPE}" \
        -DarchetypeVersion=1.4 \
        -DinteractiveMode=false

cd "${APP_NAME}"

sed -i "s/World/from ${APP_NAME}/g" src/main/webapp/index.jsp

mvn package \
	&& ls target/"${APP_NAME}".war

docker run -it --rm -p 8888:8080 --name tomcat9 -d tomcat:9.0 \
	&& docker cp target/"${APP_NAME}".war tomcat9:/usr/local/tomcat/webapps \
	&& sleep 10 \
	&& curl "http://localhost:8888/${APP_NAME}/"
<html>
<body>
<h2>Hello from myawesomeapp!</h2>
</body>
</html>
```
Nexus.  
Run nexus with podman:
```bash
podman run -d -p 8081:8081 --name nexus sonatype/nexus3
```

Login `admin`. Password is there:
```bash
podman exec -it nexus bash -c "cat /nexus-data/admin.password"
```

> [Nexus system requirenments](https://help.sonatype.com/repomanager3/product-information/system-requirements)

Go to `http://localhost:8081/#admin/security/users` and create user `myawesomeuser:myawesomepassword`.
Put login and password into `/home/$USER/.m2/settings.xml`.  
`cat /home/$USER/.m2/settings.xml`:
```xml
<settings>
  <servers>
    <server>
      <id>my-awesome-nexus-server</id>
      <username>myawesomeuser</username>
      <password>myawesomepassword</password>
    </server>
  </servers>
</settings>
```

Setting up maven to deploy app to nexus:
Add into section `properties` file `pom.xml` vesions of plugins and URL.

> Versions:
> https://mvnrepository.com/artifact/org.apache.maven.plugins/maven-deploy-plugin

`ID` (1 and 2) should equal `ID` in `/home/$USER/.m2/settings.xml` file.
```xml
<project>
  ...
  <properties>
    ...
    <nexus.url>http://localhost:8081</nexus.url>
    <plugin.maven.deploy.version>3.0.0</plugin.maven.deploy.version>
    <plugin.nexus.stading.maven.version>1.6.8</plugin.nexus.stading.maven.version>
    ...
  </properties>
    ...
  <distributionManagement>
      <repository>
          <id>my-awesome-nexus-server</id> <!-- (1) -->
          <url>${nexus.url}/repository/maven-releases/</url>
      </repository>
      <snapshotRepository>
          <id>my-awesome-nexus-server</id> <!-- (2) -->
          <url>${nexus.url}/repository/maven-snapshots/</url>
      </snapshotRepository>
  </distributionManagement>
  ...
</project>
```

Also replace `maven-deploy-plugin` with `nexus-staging-maven-plugin`. After that we can use `mvn deploy` to deploy our artifact into nexus. 
```xml
<project>
  <build>
    ...
    <plugins>
        <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-deploy-plugin</artifactId>
            <version>${plugin.maven.deploy.version}</version>
            <configuration>
                <skip>true</skip>
            </configuration>
        </plugin>
        <plugin>
            <groupId>org.sonatype.plugins</groupId>
            <artifactId>nexus-staging-maven-plugin</artifactId>
            <version>${plugin.nexus.stading.maven.version}</version>
            <executions>
                <execution>
                    <id>default-deploy</id>
                    <phase>deploy</phase>
                    <goals>
                        <goal>deploy</goal>
                    </goals>
                </execution>
            </executions>
            <configuration>
                <serverId>maven-snapshots</serverId>
                <nexusUrl>${nexus.url}/nexus/</nexusUrl>
                <skipStaging>true</skipStaging>
            </configuration>
        </plugin>
    </plugins>
  </build>
</project>
```

> More info [here](https://struchkov.dev/blog/java-jar-deploy-to-nexus/)
> https://blog.sonatype.com/2009/04/what-is-a-repository/
> https://help.sonatype.com/repomanager3/integrations/rest-and-integration-api/search-api#SearchAPI-DownloadingtheLatestVersionofanAsset
> https://stackoverflow.com/questions/57951722/how-to-remove-timestamp-from-artifacts-uploaded-to-maven2-repository-on-nexus
> http://tv-games.ru/forum/blog.php?b=2158

Try our deploy:

```bash
# < doing changes here >
sed -i 's/!/.\nIt is from Nexus!/g' src/main/webapp/index.jsp
# clean, package and deploy to registry
mvn clean package deploy
```

Go to artifact summary and pull it into variables:
- Repository: `maven-snapshots`
- Format: `maven2`
- Group: `app.awesome.my`
- Name: `myawesomeapp`
- Version: `1.0-20230207.101208-1`

```bash
REPO_LOGIN=myawesomeuser \
  && REPO_PASS=myawesomepassword \
  && REPO_NEXUS=http://localhost:8081 \
  && REPO_MAVEN=maven-snapshots \
  && APP_GROUP=app.awesome.my \
  && APP_NAME=myawesomeapp \
  && BASE_APP_VERSION=1.0-SNAPSHOT \
  && MVN_EXT=war \
  && curl -D - \
  -u $REPO_LOGIN:$REPO_PASS \
  -X GET "http://localhost:8081/service/rest/v1/search/assets/"
```
Pull artifact from registry
```bash
curl -D - -u $REPO_LOGIN:$REPO_PASS \
  --request GET -L --output "$APP_NAME.$MVN_EXT" \
  "$REPO_NEXUS/service/rest/v1/search/assets/download?sort=version&repository=$REPO_MAVEN&group=$APP_GROUP&name=$APP_NAME&maven.baseVersion=$BASE_APP_VERSION&maven.extension=$MVN_EXT"
```
Copy it into container with tomcat and get body
```bash
docker cp "$APP_NAME.$MVN_EXT" tomcat9:/usr/local/tomcat/webapps
curl "http://localhost:8888/${APP_NAME}/"
<html>
<body>
<h2>Hello from myawesomeapp.
It is from Nexus!</h2>
</body>
</html>
```
