---
author: "Sergey Chulanov"
title: "Создание swap"
date: 2021-09-18T23:28:50+03:00
description: >-
  Создание swap
tags:
  - swap
categories:
  - linux
series:
  - guide
draft: false
---

Создадим файл `swapfile`, установим соответсвующие права:

```bash
$ sudo fallocate -l 2048M /swapfile
$ sudo chown root:root /swapfile
$ sudo chmod 0600 /swapfile
$ sudo mkswap /swapfile
```

> Если создаём не файлом, а разделом, то нужно выбрать тип раздела `Linux swap`

Если используется более чем один `swap` файл или раздел, следует указать значение приоритета. Файл или раздел с большим приоритетом будет задействован раньше. Значения можно выбирать из диапазона от 0 до 32767. Сначала будет использоваться `swap`-область с большим значением. Например, если есть 2 диска: `sda - быстрый`, а `sdb - медленный`, то есть смысл в `/etc/fstab` добавить следующие строки:

```bash
/dev/sda1 none swap defaults,pri=100 0 0
/dev/sdb2 none swap defaults,pri=10  0 0
```

Можно посмотреть, какие приоритеты у `swap`-областей установлены сейчас в системе:

```bash
$ swapon --summary
Filename                Type      Size      Used    Priority
/dev/sdb2               partition 29856032  1878272 20
/home/user/myswapfile   file      10485756  7608668 10
```

Добавим созданную `swap`-область:
```bash
$ swapon --priority 100 /swapfile
```

И добавим в `/etc/fstab`, чтобы область монтировалась после перезагрузки:
```bash
$ echo "/swapfile swap swap defaults,pri=100 0 0" | sudo tee --append /etc/fstab
```
