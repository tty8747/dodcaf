---
author: "Sergey Chulanov"
title: "Работа с записями route53 с помощью утилиты aws."
date: 2021-07-29T18:23:29+03:00
description: >-
  Описание как удалить/добавить/изменить dns-записи в route53 от aws,
  используя aws cli
tags:
  - aws
categories:
  - cli
series:
  - guide
draft: false
---

Для начала опишу установку. Скачиваем, распаковываем и устанавливаем утилиту `aws`:  

```bash
$ curl https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip -o awscliv2.zip
$ unzip awscliv2.zip
$ sudo ./aws/install
```

Проверим версию - убедимся что утилита работает:

```bash
$ aws --version
aws-cli/2.2.23 Python/3.8.8 Linux/5.8.0-63-generic exe/x86_64.ubuntu.20 prompt/off
```

> Более подробно об установке / удалении / обновлении на сайте [docs.aws.amazon.com](https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2-linux.html#cliv2-linux-install)

Создадим профиль:  

```bash
$ aws configure --profile myaws
```

Посмотрим список профилей:  

```bash
$ aws configure list --profile myaws
      Name                    Value             Type    Location
      ----                    -----             ----    --------
   profile                    myaws           manual    --profile
access_key     ****************4VMU shared-credentials-file
secret_key     ****************utju shared-credentials-file
    region             eu-central-1      config-file    ~/.aws/config
```

Если профилей несколько, то профиль по-умолчанию можно выбрать, установив переменную окружения:

```bash
$ export AWS_DEFAULT_PROFILE=<profile name>
```

Так же можно наполнять файл конфигурации `~/.aws/config` следующим образом:

```bash
$ aws configure set varname value [--profile profile-name]
```

> С помощью этих команд можно так же изменить файлы конфигурации `aws`.

Например, если есть пустой файл конфигурации, то эти команды:

```bash
$ aws configure set aws_access_key_id default_access_key
$ aws configure set aws_secret_access_key default_secret_key
$ aws configure set default.region us-west-2
$ aws configure set default.ca_bundle /path/to/ca-bundle.pem
$ aws configure set region us-west-1 --profile testing
$ aws configure set profile.testing2.region eu-west-1
$ aws configure set preview.cloudsearch true
```

приведут его с следующему виду:
```bash
[default]
region = us-west-2
ca_bundle = /path/to/ca-bundle.pem

[profile testing]
region = us-west-1

[profile testing2]
region = eu-west-1

[preview]
cloudsearch = true
```

и файл `~/.aws/credentials` примет вид:

```bash
[default]
aws_access_key_id = default_access_key
aws_secret_access_key = default_secret_key
```

> Так же можно вручную отредактировать эти файлы.

Теперь посмотрим, какие dns-зоны доступны:

```bash
$ aws route53 list-hosted-zones --profile myaws
{
    "HostedZones": [
        {
            "Id": "/hostedzone/LCDKBBJ8UMH0Y",
            "Name": "test1.dodcaf.ru.",
            "CallerReference": "44BFH85B-6745-AF06-BECE-5769C81HHDFH",
            "Config": {
                "PrivateZone": false
            },
            "ResourceRecordSetCount": 340
        }
    ]
}
```

В качестве примера удалим запись `nginx01.test1.dodcaf.ru.`, для этого нужно создать следующий файл:

```bash
$ cat delete.json
{
    "Comment": "CREATE/DELETE/UPSERT a record" ,
    "Changes": [
        {
            "Action": "DELETE",
            "ResourceRecordSet": {
                "Name": "nginx01.test1.dodcaf.ru.",
                "Type": "A",
                "TTL": 300,
                "ResourceRecords": [
                    {
                        "Value": "95.217.153.145"
                    }
                ]                
            }
        }
    ]
}
```

Теперь остаётся только применить:

```bash
$ aws route53 change-resource-record-sets --hosted-zone-id LCDKBBJ8UMH0Y --change-batch file://delete.json --profile myaws
```

### Полезные ссылки:
  - https://gist.github.com/apolloclark/b3f60c1f68aa972d324b
