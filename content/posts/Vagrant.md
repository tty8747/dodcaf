---
author: "Sergey Chulanov"
title: "Vagrant"
date: 2023-08-13T11:54:22+03:00
description: >-
  some description
tags:
  - vagrant
categories:
  - linux
  - devops
  - ops
  - guide
series:
  - guide
draft: false
---

### Quick links

| Ubuntu 22.04

## Install vagrant and plugin for `libvirt`:
You should install [libvirt](https://www.dodcaf.ru/posts/ubuntu-as-a-gateway/#%D1%83%D1%81%D1%82%D0%B0%D0%BD%D0%BE%D0%B2%D0%B8%D1%82%D1%8C-%D0%B2%D0%B8%D1%80%D1%82%D1%83%D0%B0%D0%BB%D0%B8%D0%B7%D0%B0%D1%86%D0%B8%D1%8E-libvrt)

```bash
sudo apt install vagrant
sudo vagrant plugin install vagrant-libvirt
```

> You can run `export VAGRANT_DEFAULT_PROVIDER=libvirt` to choose default driver.

Get ubuntu image and init Vagrantfile:
```bash
vagrant box add generic/ubuntu2004 --provider=libvirt
vagrant box add generic/ubuntu2204 --provider=libvirt
vagrant box list
vagrant box remove generic/ubuntu2204
vagrant init generic/ubuntu2004
```

> - [VM provider configuration](https://vagrant-libvirt.github.io/vagrant-libvirt/configuration.html)
> - [VM provider network configuration](https://vagrant-libvirt.github.io/vagrant-libvirt/configuration.html#private-network-options)
> - [VM configs and more](https://developer.hashicorp.com/vagrant/docs/vagrantfile/machine_settings)
> - [Examples](https://vagrant-libvirt.github.io/vagrant-libvirt/examples.html)
> - [Tips](https://developer.hashicorp.com/vagrant/docs/vagrantfile/tips)
> - [ENV plugin](https://github.com/gosuri/vagrant-env)

Change setting and run:
> To validate config use: `vagrant validate`
```bash
Vagrant.configure("2") do |config|
  config.vm.box = "generic/ubuntu2004"
  config.vm.define "ceph1" do |t|
  end
  config.vm.provider "libvirt" do |l|
  # l.title = 'ceph1'
  # l.description = 'ceph1 for test purposes'
    l.cpus = 4
    l.cputopology :sockets => '2', :cores => '2', :threads => '1'
    l.memory = 4096
    l.disk_driver :cache => 'unsafe'
  end
  config.vm.network "private_network", ip: "192.168.33.10"
  config.vm.network "private_network", ip: "192.168.44.10"

  config.vm.provision "shell", inline: <<-SHELL
    apt-get update
  SHELL
end

```

```bash
vagrant up
vagrant ssh
```

Run `vagrant ssh-config` to get info to connect:
```bash
Host ceph1
  HostName 192.168.121.82
  User vagrant
  Port 22
  UserKnownHostsFile /dev/null
  StrictHostKeyChecking no
  PasswordAuthentication no
  IdentityFile /tmp/1/.vagrant/machines/ceph1/libvirt/private_key
  IdentitiesOnly yes
  LogLevel FATAL
```

> According to previos: `ssh 192.168.121.82 -i /tmp/1/.vagrant/machines/ceph1/libvirt/private_key -lvagrant`

```bash
virsh list --all
vagrant destroy -f
```

> Had this error today on mac and decided to update this post with the solution that worked for me.
> Steps:
> - Delete all redundant machine folders `./.vagrant/machines` (`.vagrant` folder is a hidden folder within the repo)
> - Run `vagrant global-status --prune` command in the root of the project
> - Destroy vagrant setup `vagrant destroy`
> - Ensure no related machine exists in the virtualbox/libvirtd UI
> - Run `vagrant up` again

## Simple config for several nodes:
```bash
Vagrant.configure("2") do |config|
  config.ssh.insert_key = false
  config.vm.define "master" do |master|
    master.vm.box = "generic/ubuntu2004"
  end
  config.vm.define "worker" do |worker|
    worker.vm.box = "generic/ubuntu2004"
  end
  # config.vm.network "public_network"
  config.vm.provider "libvirt" do |l|
    l.cpus = 2
    l.cputopology :sockets => '2', :cores => '1', :threads => '1'
    l.memory = 2048
    l.disk_driver :cache => 'unsafe'
  end
end
```

## Also config with two nodes with private network:
```bash
Vagrant.configure("2") do |config|

  config.vm.box = "generic/ubuntu2004"

  config.vm.define "web" do |web|
    web.vm.network "private_network", ip: "192.168.50.3"
    web.vm.network "forwarded_port", guest: 80, host: 8083

    web.vm.provider "libvirt" do |l|
      l.cpus = 2
      l.memory = 512
    end
  end

  config.vm.define :sql01 do |sql|
    sql.vm.network "private_network", ip: "192.168.50.2"
    sql.vm.network "forwarded_port", guest: 80, host: 8084

    sql.vm.provider "libvirt" do |l|
      l.cpus = 4
      l.memory = 4096
    end
  end

  # https://developer.hashicorp.com/vagrant/docs/synced-folders/basic_usage
  # map /home/goto/samples/2nginx -> /srv/website
  config.vm.synced_folder "/home/goto/samples/2nginx", "/srv/website"

end
```

## Create several nodes with public network

But you need to [install bridge](https://www.dodcaf.ru/posts/bridge-with-nmcli-and-kvm/)

```bash
Vagrant.configure("2") do |config|

  config.vm.box = "generic/ubuntu2004"

  config.vm.define "web" do |web|
    web.vm.network "private_network", ip: "192.168.50.3"
    web.vm.network "forwarded_port", guest: 80, host: 8083

    web.vm.network :public_network,
      :dev => "br0",
      :mode => "bridge",
      :type => "bridge"

    web.vm.provider "libvirt" do |l|
      l.cpus = 2
      l.memory = 512
    end
  end

  config.vm.define :sql01 do |sql|
    sql.vm.network "private_network", ip: "192.168.50.2"
    sql.vm.network "forwarded_port", guest: 80, host: 8084

    sql.vm.network :public_network,
      :dev => "br0",
      :mode => "bridge",
      :type => "bridge"

    sql.vm.provider "libvirt" do |l|
      l.cpus = 4
      l.memory = 4096
    end
  end

  config.vm.provision "file", source: "~/.ssh/id_rsa.pub", destination: "~/.ssh/authorized_keys"
  config.vm.provision "shell", inline: <<-SHELL
    apt-get update
  SHELL

  # https://developer.hashicorp.com/vagrant/docs/synced-folders/basic_usage
  # map /home/goto/samples/2nginx -> /srv/website
  config.vm.synced_folder "/home/goto/samples/2nginx", "/srv/website"

end
```

Prepare 3 nodes with ansible provisioning:
```bash
ENV["LC_ALL"] = "en_US.UTF-8"

Vagrant.configure("2") do |config|

  config.vm.box = "generic/ubuntu2004"
  config.vm.provision "file", source: "~/.ssh/id_rsa.pub", destination: "/tmp/authorized_keys"
  config.vm.provision "shell", inline: <<-SHELL
    apt-get update
    useradd goto -s /bin/bash -m -G sudo
    echo "goto  ALL=(ALL:ALL) NOPASSWD: ALL" >> /etc/sudoers
    cp -Rv ~vagrant/.ssh ~goto/.ssh
    mv -v /tmp/authorized_keys ~goto/.ssh/authorized_keys
    chown -Rv goto:goto ~goto/.ssh
  SHELL

  (1..3).each do |i|
    config.vm.define "ceph#{i}" do |ceph|
      ceph.vm.hostname = "ceph#{i}"
      ceph.vm.network "private_network", ip: "192.168.7.#{20+i}"
      ceph.vm.network "private_network", ip: "192.168.8.#{20+i}"
      ceph.vm.network "private_network", ip: "192.168.9.#{20+i}"
      ceph.vm.provider "libvirt" do |l|
        l.cpus = 1
        l.memory = 512
      end
      if ceph.vm.hostname == "ceph15"
        config.vm.provision :ansible do |ansible|
          ansible.playbook       = "init.yml"
          ansible.verbose        = true
          ansible.limit          = "all"
          ansible.inventory_path = "inventory"
        end
      end
    end
  end
  config.vm.provision :ansible do |ansible|
    ansible.playbook       = "/path/to/your/main/yaml/or/init.yml"
    ansible.verbose        = true
  end
end
```

```bash
vagrant up
vagrant provision
# for one node:
# ssh "$(vagrant ssh-config | grep HostName | cut -c12- )" "cat /etc/*release"
# for several nodes:
for ip in $(vagrant ssh-config | grep HostName | cut -c12- ); do ssh "$ip" "cat /etc/*release"; printf "\n"; done
vagrant destroy
```

> [Two ubuntu nodes with docker, portainer & Kubernetes cluster in less than a minute](https://medium.com/devops-dudes/how-to-setup-vagrant-and-virtual-box-for-ubuntu-20-04-7374bf9cc3fa)
