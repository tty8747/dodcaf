---
author: "Sergey Chulanov"
title: "Get absolute path for a file"
date: 2023-08-10T21:52:20+03:00
description: >-
  locate a file
tags:
  - useful
categories:
  - linux
  - devops
series:
  - guide
draft: false
---

- We need to install `mlocate`:
```bash
apt install mlocate
```
- Update the db:
```bash
sudo updatedb
```
And find some file:
```bash
locate just-file-name.txt
locate or-part-of-file-nam
```

Example:
```bash
locate mcypr_lin___ovpn.ovpn
  /etc/openvpn/kz/mcypr_lin___ovpn.ovpn
```
