---
author: "Sergey Chulanov"
title: "xsel for buffer"
date: 2023-08-10T15:46:55+03:00
description: >-
  Useful thing
tags:
  - useful
categories:
  - linux
  - devops
series:
  - guide
draft: false
---

Install package to save file context into clipboard buffers:
```bash
sudo apt install xsel
```

To save in the primary (to paste: `Shift + Insert`, `xsel -po`):
```bash
cat /etc/hosts | xsel -ip
```
To save in the secondary (to paste: `xsel -so`)
```bash
cat /etc/hosts | xsel -is
```

`xsel -x` - Exchange the PRIMARY and SECONDARY selections  

To save in the clipboard (to paste: `Ctrl + V`, `xsel -bo`)
```bash
cat /etc/hosts | xsel -ib
```

> From man:
> ```bash
> Selection options
> -p, --primary         Operate on the PRIMARY selection (default)
> -s, --secondary       Operate on the SECONDARY selection
> -b, --clipboard       Operate on the CLIPBOARD selection
