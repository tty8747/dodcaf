---
author: "Sergey Chulanov"
title: "Run Windows bin as a service"
date: 2023-03-22T09:44:22+03:00
description: >-
  Run windows exe file as a service
tags:
  - windows
categories:
  - windows
  - devops
  - guide
series:
  - guide
draft: false
---

### Quick links

| Windows

- Download zss-file nssm from http://nssm.cc/download, unpack it into `C:\nssm`
- Run `C:\nssm\win64\nssm.exe` and choose, any binary, for example `traefik.exe`
- Open these ports in windows firewall

Powershell commands for widnows firewall
```powershell
New-NetFirewallRule -Name 'tcp80' -DisplayName 'tcp80' -Enabled True -Direction Inbound -Protocol TCP -Action Allow -LocalPort 80
New-NetFirewallRule -Name 'tcp443' -DisplayName 'tcp443' -Enabled True -Direction Inbound -Protocol TCP -Action Allow -LocalPort 443

Get-NetFirewallRule -DisplayName tcp80
Get-NetFirewallRule -Name tcp80
Get-NetFirewallPortFilter | Where-Object -Property LocalPort -EQ 80
Remove-NetFirewallRule -Name tcp80
```
