---
author: "Sergey Chulanov"
title: "Настройка локали в Ubuntu."
date: 2021-08-03T12:53:01+03:00
description: >-
  Описание как удалить/добавить/изменить dns-записи в route53 от aws,
  используя aws cli
tags:
  - locale
categories:
  - linux
series:
  - guide
draft: false
---

Посмотрим установленные локали:

```bash
$ locale -a
C
C.UTF-8
POSIX
```

Мне нужны 2 локали `en_US.utf8` и `ru_RU.utf8`.
Для этого нужно раскомментировать в файле `/etc/locale.gen` следующие строки:

```bash
en_US.UTF-8 UTF-8
ru_RU.UTF-8 UTF-8
```

Теперь достаточно выполнить команду:

```bash
$ sudo locale-gen
Generating locales (this might take a while)...
  en_US.UTF-8... done
  ru_RU.UTF-8... done
Generation complete.
```

2й вариант такой:

```bash
$ locale-gen en_US.UTF-8
# либо
$ localedef -i en_US -f UTF-8 en_US.UTF-8
```

Проверим, снова и убедимся, что локали присутствуют:

```bash
$ locale -a
C
C.UTF-8
POSIX
en_US.utf8
ru_RU.utf8
```

Посмотрим, какие локали заданы в системе:

```bash
$ locale
LANG=C.UTF-8
LANGUAGE=
LC_CTYPE="C.UTF-8"
LC_NUMERIC=C.UTF-8
LC_TIME=C.UTF-8
LC_COLLATE="C.UTF-8"
LC_MONETARY=C.UTF-8
LC_MESSAGES=C.UTF-8
LC_PAPER=C.UTF-8
LC_NAME=C.UTF-8
LC_ADDRESS=C.UTF-8
LC_TELEPHONE=C.UTF-8
LC_MEASUREMENT=C.UTF-8
LC_IDENTIFICATION=C.UTF-8
LC_ALL=
```

- `LC_ALL` - если установлена, то используется для всех категорий локалей, даже если они заданы;
- `LANG` - используется для тех категорий локалей, для которых значение не задано;
- `LANGUAGE` - если установлена, то используется вместо LC_MESSAGES.
- `LC_MESSAGES` - язык, на котором будут выводиться ошибки. (Если задана `LANGUAGE`, то будет использоваться она)

Это совсем не то, что мне нужно. Исправим ситуацию:

```bash
sudo update-locale \
    LANG="en_US.UTF-8" \
    LC_CTYPE="ru_RU.UTF-8" \
    LC_NUMERIC="ru_RU.UTF-8" \
    LC_TIME="ru_RU.UTF-8" \
    LC_COLLATE="ru_RU.UTF-8" \
    LC_MONETARY="ru_RU.UTF-8" \
    LC_MESSAGES="en_US.UTF-8" \
    LC_PAPER="ru_RU.UTF-8" \
    LC_NAME="ru_RU.UTF-8" \
    LC_ADDRESS="ru_RU.UTF-8" \
    LC_TELEPHONE="ru_RU.UTF-8" \
    LC_MEASUREMENT="ru_RU.UTF-8" \
    LC_IDENTIFICATION="ru_RU.UTF-8"
```

2й вариант установки локалей, через export переменных:

```bash
export LANGUAGE=en_US.UTF-8
export LANG=ru_RU.UTF-8
export LC_ALL=en_US.UTF-8
locale-gen en_US.UTF-8
dpkg-reconfigure locales
```

Убедимся, что всё применилось как надо:

```bash
$ locale
LANG=en_US.UTF-8
LANGUAGE=
LC_CTYPE=ru_RU.UTF-8
LC_NUMERIC=ru_RU.UTF-8
LC_TIME=ru_RU.UTF-8
LC_COLLATE=ru_RU.UTF-8
LC_MONETARY=ru_RU.UTF-8
LC_MESSAGES=en_US.UTF-8
LC_PAPER=ru_RU.UTF-8
LC_NAME=ru_RU.UTF-8
LC_ADDRESS=ru_RU.UTF-8
LC_TELEPHONE=ru_RU.UTF-8
LC_MEASUREMENT=ru_RU.UTF-8
LC_IDENTIFICATION=ru_RU.UTF-8
LC_ALL=
```

> Полезные ссылки:  
> - [edu.postgrespro.ru](https://edu.postgrespro.ru/dba2/dba2_16_admin_localization.html)
