---
author: "Sergey Chulanov"
title: "Install, backup and restore Neo4j database"
date: 2023-05-19T10:17:45+03:00
description: >-
  Install, backup and restore neo4j database"
tags:
  - neo4j
categories:
  - linux
  - devops
  - ops
  - databases
  - neo4j
  - guide
series:
  - guide
draft: false
---

### Quick links

| Ubuntu 22.04 | [Installation](#installation) | [Some activities with `neo4j` database](#some-activities-with-neo4j-database) | [Backup `neo4j` database](#backup-neo4j-database) | [Restore `neo4j` database](#restore-neo4j-database)

> Before install you have to check your `OpenJDK version`, for example for `neo4j 5.8.0` you should have `openjdk 17.0.7 2023-04-18`.

## Installation:

> [Some article who helps install it.](https://www.techrepublic.com/article/how-to-install-neo4j-ubuntu-server/)

Get `gpg.key` and add neo4j repository:
```bash
curl -fsSL https://debian.neo4j.com/neotechnology.gpg.key | sudo gpg --dearmor -o /usr/share/keyrings/neo4j.gpg
echo "deb [signed-by=/usr/share/keyrings/neo4j.gpg] https://debian.neo4j.com stable latest" | sudo tee -a /etc/apt/sources.list.d/neo4j.list
sudo apt update
```

Install needed `java` version:
```bash
# for neo4j 5.8.0
sudo apt install openjdk-17-jre
java --version
```

If you have several versions of `java` set needed version:
```bash
sudo update-alternatives --config java
```

And, finally, install `neo4j` and `cypher-shell`:
```bash
sudo apt install neo4j cypher-shell -y
```

Change these lines in `/etc/neo4j/neo4j.conf`
```bash
server.default_listen_address=0.0.0.0

# Bolt connector
server.bolt.enabled=true
server.bolt.tls_level=DISABLED
server.bolt.listen_address=:7687
#server.bolt.advertised_address=:7687

# HTTP Connector. There can be zero or one HTTP connectors.
server.http.enabled=true
server.http.listen_address=:7474
#server.http.advertised_address=:7474
```

Set password and start `neo4j`:
```bash
sudo -u neo4j neo4j-admin dbms set-initial-password your_password
sudo systemctl enable --now neo4j
```

> Don't forget open ports in your firewall!
>[Recover a password if you forgot it](https://neo4j.com/docs/operations-manual/current/configuration/password-and-user-recovery/)

## Some activities with `neo4j` database

> `neo4j-admin` should from `neo4j` user
> Fix if you broke permissions: `sudo chown -R neo4j:neo4j /var/lib/neo4j`

Connect:
```bash
cypher-shell -a ubu22.home:7687 -u neo4j -p your_password
```
http link: http://ubu22.home:7474/browser/

Show databases and create data:
> Helpful information from [digitalocean.com](https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-neo4j-on-ubuntu-22-04#step-4-using-neo4j)

```bash
show databases;
show database neo4j;
SHOW DATABASE neo4j YIELD *;

CREATE (:Shark {name: 'Great White'});

CREATE
(:Shark {name: 'Hammerhead'})-[:FRIEND]->
(:Shark {name: 'Sammy'})-[:FRIEND]->
(:Shark {name: 'Megalodon'});

MATCH (a:Shark),(b:Shark)
WHERE a.name = 'Sammy' AND b.name = 'Megalodon'
CREATE (a)-[r:ORDER { name: 'Lamniformes' }]->(b)
RETURN type(r), r.name;

MATCH (a:Shark),(b:Shark)
WHERE a.name = 'Sammy' AND b.name = 'Hammerhead'
CREATE (a)-[r:SUPERORDER { name: 'Selachimorpha'}]->(b)
RETURN type(r), r.name;

MATCH (a)-[r]->(b)
RETURN a.name,r,b.name
ORDER BY r;
```

## Backup `neo4j` database
> [Official link](https://neo4j.com/docs/operations-manual/current/backup-restore/offline-backup/#offline-backup-command)

```bash
sudo mkdir -pv /var/backups/neo4jbackups && sudo chown neo4j:neo4j /var/backups/neo4jbackups
sudo systemctl stop neo4j
sudo -u neo4j bash
neo4j-admin database check neo4j
neo4j-admin database dump --to-path=/var/backups/neo4jbackups neo4j
    2023-05-19 07:11:08.400+0000 INFO  [o.n.c.d.DumpCommand] Starting dump of database 'neo4j'
    Done: 36 files, 257.9MiB processed.
    2023-05-19 07:11:08.977+0000 INFO  [o.n.c.d.DumpCommand] Dump completed successfully
exit
sudo systemctl start neo4j
```

> In a neo4j docker installation, the neo4j-admin restore requires the neo4j service to be stopped as one can’t do a restore on a running database. [More information is here.](https://neo4j.com/developer/kb/stopping-and-restoring-neo4j-docker-image/)

## Restore `neo4j` database
> [Official link](https://neo4j.com/docs/operations-manual/current/backup-restore/restore-dump/)

```bash
sudo systemctl stop neo4j
sudo -u neo4j bash
neo4j-admin database load --from-path=/var/backups/neo4jbackups --overwrite-destination=true neo4j
    Done: 36 files, 257.9MiB processed.
neo4j-admin database check neo4j
exit
sudo systemctl start neo4j
```
