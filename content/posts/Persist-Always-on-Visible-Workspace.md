---
author: "Sergey Chulanov"
title: "Persist Always on Visible Workspace"
date: 2025-01-31T13:39:17+03:00
description: >-
  Persist Always on Visible Workspace
tags:
  - xorg
categories:
  - linux
series:
  - guide
draft: false
---

| Ubuntu 22.04


> It doesn't work on Wayland, only on Xorg

Install devilspie2 an create test config for Mozilla Firefox
```bash
sudo apt install devilspie2
mkdir -pv ~/.config/devilspie2

cat <<EOF > /home/goto/.config/devilspie2/firefox.lua
  if (string.match(get_window_name(), "Mozilla Firefox")) then
      maximize();
      stick_window();
  end
EOF

devilspie2 --debug
```

Mozilla Firefox should open in maximize and be visible on all workspaces  

Find name of the windows:
```bash
xwininfo -tree -root
xwininfo -tree -root | grep -i 1password
```

Use names you've got to create config for apps:
```bash
cat <<EOF > /home/goto/.config/devilspie2/myapps.lua 
  if (string.match(get_window_name(), "Telegram")) then
      stick_window();
  end
  
  if (string.match(get_window_name(), "Mail")) then
      stick_window();
  end
  
  if (string.match(get_window_name(), "Postman")) then
      stick_window();
  end
  
  if (string.match(get_window_name(), "1Password")) then
      stick_window();
  end
EOF
```

More options in README.md
```bash
gzip --decompress /usr/share/doc/devilspie2/README.gz --stdout | less
```

Create `systemd-unit` for user
```bash
cat << EOF > /home/goto/.config/systemd/user/devilspie2.service
[Unit]
Description=devilspie2
Wants=display-manager.service
After=display-manager.service

[Service]
Type=simple
# Next line is only required for Xorg, should be removed for Wayland
Environment=XAUTHORITY=%h/.Xauthority
ExecStart=/usr/bin/devilspie2
ExecStop=/usr/bin/pkill devilspie2
RemainAfterExit=yes

[Install]
WantedBy=graphical-session.target
EOF
```

Enable it to run everytime
```bash
systemctl enable --now devilspie2.service --user
systemctl status devilspie2.service --user
```
