---
author: "Sergey Chulanov"
title: "Import and export MongoDB collections"
date: 2023-05-14T15:04:56+03:00
description: >-
  How to import and export collections on MongoDB.
tags:
  - mongo
categories:
  - linux
  - devops
  - databases
  - nosql
  - guide
series:
  - guide
draft: false
---

### Quick links

| Ubuntu 22.04 | [Mongo import](#import) | [Mongo export](#export)

Prepare a mongo server instance:
```bash
docker run --rm --name mongo -d mongo:6
docker exec -it -w /root mongo bash
apt update && apt -y install curl less jq
```

> We are in the container, so all command will run from root!

## Import

MongoDB is one of the most popular NoSQL database engines. For our test we can take the most popularsample MongoDB database about restaurants:
```bash
curl -LO https://raw.githubusercontent.com/mongodb/docs-assets/primer-dataset/primer-dataset.json
head primer-dataset.json | jq '.'
```

Let's import it into database `mydb` and collection `restaurants`  using `mongoimport`. Mongo will create :
```bash
mongoimport --db mydb --collection restaurants --file primer-dataset.json
  2023-05-14T13:19:13.396+0000  connected to: mongodb://localhost/
  2023-05-14T13:19:14.537+0000  25359 document(s) imported successfully. 0 document(s) failed to import.
```

Connect to out `mydb`:

```bash
mongosh mydb
```

Count the documents in collection `restaurants` and check a document in `restaurants`:
```bash
db.restaurants.countDocuments()
  25359
db.restaurants.findOne()
  {
    _id: ObjectId("6460dfd11e50c976dc94fa60"),
    address: {
      building: '1007',
  ...
exit
```

## Export

We should use `mongoexport` to export the `restaurants` collection from the `mydb` database which we have previously imported
```bash
mongoexport --db mydb -c restaurants --out mydbexport.json
```

If you need to get a part of our collection. For example, let's find all the restaurants which satisfy the criteria to be situated in the Bronx borough and to have Italian cuisine:
```bash
mongosh mydb
db.restaurants.find( { "borough": "Bronx", "cuisine": "Italian" } )
exit
mongoexport --db mydb -c restaurants -q "{\"borough\": \"Bronx\", \"cuisine\": \"Italian\"}" --out Bronx_Italian_restaurants.json
less Bronx_Italian_restaurants.json
cat Bronx_Italian_restaurants.json | jq '.'
```

> Usefull link [here](https://www.digitalocean.com/community/tutorials/how-to-import-and-export-a-mongodb-database-on-ubuntu-20-04)