---
author: "Sergey Chulanov"
title: "Добавление дополнительного пространства к виртуальному диску."
date: 2021-10-07T09:50:48+03:00
description: >-
  Добавление дополнительного пространства к виртуальному диску.
tags:
  - lvm
  - virsh
categories:
  - linux
series:
  - guide
draft: false
---


| Ubuntu 20.04 | lvm | ext4 | libvirtd | virsh |

## Чтобы начать нужно убедиться, что не создано снапшотов этой виртуалки, либо удалить все созданные.

```bash
$ sudo virsh snapshot-list --domain ubuntu20.04
$ sudo virsh snapshot-delete --domain ubuntu20.04 --snapshotname before3sep
```

## Увеличим объём виртуального диска.

Необходимо определить расположение диска:

```bash
$ sudo virsh domblklist --domain ubuntu20.04
```

Добавим 10G:

```bash
$ sudo qemu-img resize /var/lib/libvirt/images/ubuntu20.04.qcow2 +10G
```

Запускаем виртуалку:

```bash
$ sudo virsh start --domain ubuntu20.04
```

Заходим в виртуалку и смотрим объём. Объём `vda` увеличился, но на `vda3` нет.

```bash
lsblk /dev/vda
vda                       252:0    0    25G  0 disk
├─vda1                    252:1    0     1M  0 part
├─vda2                    252:2    0     1G  0 part /boot
└─vda3                    252:3    0    14G  0 part
  └─ubuntu--vg-ubuntu--lv 253:0    0    14G  0 lvm  /
```

Вызов `sudo fdisk /dev/vda` сообщает, что после сохрания размер скорректируется:
```
GPT PMBR size mismatch (31457279 != 52428799) will be corrected by write.
The backup GPT table is not on the end of the device. This problem will be corrected by write.
```

Сохраняем.
Не сработало... Удалим раздел и пересоздадим заново, не удаляя LVM сигнатуру:

```bash
root@k8s:~# fdisk /dev/vda

Welcome to fdisk (util-linux 2.34).
Changes will remain in memory only, until you decide to write them.
Be careful before using the write command.


Command (m for help): p
Disk /dev/vda: 25 GiB, 26843545600 bytes, 52428800 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: gpt
Disk identifier: BBEE7B38-20CE-440A-B39C-8FE5A7356C98

Device       Start      End  Sectors Size Type
/dev/vda1     2048     4095     2048   1M BIOS boot
/dev/vda2     4096  2101247  2097152   1G Linux filesystem
/dev/vda3  2101248 31455231 29353984  14G Linux filesystem

Command (m for help): d
Partition number (1-3, default 3): 3

Partition 3 has been deleted.

Command (m for help): n
Partition number (3-128, default 3):
First sector (2101248-52428766, default 2101248):
Last sector, +/-sectors or +/-size{K,M,G,T,P} (2101248-52428766, default 52428766):

Created a new partition 3 of type 'Linux filesystem' and of size 24 GiB.
Partition #3 contains a LVM2_member signature.

Do you want to remove the signature? [Y]es/[N]o: N

Command (m for help): p

Disk /dev/vda: 25 GiB, 26843545600 bytes, 52428800 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: gpt
Disk identifier: BBEE7B38-20CE-440A-B39C-8FE5A7356C98

Device       Start      End  Sectors Size Type
/dev/vda1     2048     4095     2048   1M BIOS boot
/dev/vda2     4096  2101247  2097152   1G Linux filesystem
/dev/vda3  2101248 52428766 50327519  24G Linux filesystem

Command (m for help): w
```

Смотрим снова, вроде бы всё окей:
```bash
root@k8s:~# lsblk /dev/vda
NAME                      MAJ:MIN RM SIZE RO TYPE MOUNTPOINT
vda                       252:0    0  25G  0 disk
├─vda1                    252:1    0   1M  0 part
├─vda2                    252:2    0   1G  0 part /boot
└─vda3                    252:3    0  24G  0 part
  └─ubuntu--vg-ubuntu--lv 253:0    0  14G  0 lvm  /
```

Далее, для `lvm` расширяем `physical volume` до максимума:
```bash
$ sudo pvresize /dev/vda3
```
Проверяем и убеждаемся что объём стал доступен:
```bash
$ sudo pvdisplay
$ sudo vgdisplay
```
Изменим размер `logical volume`, используя максимум доступного места:
```bash
$ sudo lvextend -l +100%FREE /dev/ubuntu-vg/ubuntu-lv
```

Растянем файловую систему на доступный объём:
```bash
$ sudo resize2fs /dev/ubuntu-vg/ubuntu-lv
```

Проверяем, всё окей!
```bash
$ df -BG /
Filesystem                        1G-blocks  Used Available Use% Mounted on
/dev/mapper/ubuntu--vg-ubuntu--lv       24G   12G       11G  54% /
```
