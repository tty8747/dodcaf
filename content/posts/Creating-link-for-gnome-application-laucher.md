---
author: "Sergey Chulanov"
title: "Creating link for gnome application launcher"
date: 2024-04-29T22:12:23+03:00
description: >-
  Creating link for gnome application launcher (example for xelis wallet (genesix)
tags:
  - gnome
categories:
  - linux
series:
  - guide
draft: false
---

| Ubuntu 22.04

> [Xelis wallet](https://xelis.io/)

Download the package and extract:
```bash
VER=0.0.2
curl -Lo /tmp/genesix_linux.zip https://github.com/xelis-project/xelis-genesix-wallet/releases/download/$VER/genesix_linux.zip
unzip -d ~/Apps/xel/ /tmp/genesix_linux.zip
```

We can download the icon and put it here as we will need to make desktop icon later
For example save it from https://xelis.io/favicon.ico on `/home/goto/Apps/xel/` with name `favicon.ico`

To make desktop icon for `xel`, we can make a file in usr/share/applications:
```bash
cat <<EOF > /usr/share/applications/xel.desktop
[Desktop Entry]
Encoding=UTF-8
Type=Application
Name=xel
Icon=/home/goto/Apps/xel/favicon.ico
Exec="/home/goto/Apps/xel/bundle/genesix"
Comment=xel-genesis
Categories=Wallet;
Terminal=false
StartupNotify=true
EOF
```

Now, we can find the icon in application launcher menu by search for `xel`
