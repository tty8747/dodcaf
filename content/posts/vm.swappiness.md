---
author: "Sergey Chulanov"
title: "Заметка о vm.swappiness"
date: 2021-09-17T10:37:49+03:00
description: >-
  Как сделать так, чтобы swap-раздел вступал в игру позже.
tags:
  - swap
categories:
  - linux
series:
  - guide
draft: false
---

`vm.swappiness` задаёт процент свободной оперативной памяти при превышении которого активируется `swap`.

По-умолчанию vm.swappiness = 60 - это компромисс для десктопных систем, т.е. `swap` подключится, когда будет занято 40% памяти.
Согласно [Red Hat Performance Tuning manual](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/performance_tuning_guide/s-memory-tunables) следует использовать *значение 10* для серверов. Меньшее значение увеличивает риск того, что придёт OOM killer.

Проверим текущее значение:

```bash
$ cat /proc/sys/vm/swappiness    
60
$ sysctl -a | grep swappiness
vm.swappiness = 60
```

Для того, чтобы изменить это положение дел, нужно добавить в файл `/etc/sysctl.conf` строку `vm.swappiness=5`. Если следовать рекомендациям в файле `/etc/sysctl.d/README.sysctl`, то правильным выбором будет создание файла в директории `/etc/sysctl.d/*.conf`. 

Для локальных изменений создим файл `/etc/sysctl.d/local.conf` со строкой `vm.swappiness=5` и применим изменения:

```bash
$ sudo sysctl -p  --load /etc/sysctl.d/local.conf
```

Проверим текущее значение снова:

```bash
$ cat /proc/sys/vm/swappiness    
5
$ sysctl -a | grep swappiness
vm.swappiness = 5
```

При нагрузке на систему следует обратить внимание на параметр `swout` в `atop` и на то, как долго висит большое значение в `swout`.
Посмотрим с интервалом в 1 секунду:
```bash
$ atop aA 1
```

> [man atop](https://linux.die.net/man/1/atop):
> If the number of pages swapped out ('swout' in the PAG-line) is larger than 10 per second, the memory resource is considered 'critical'. A value of at least 1 per second is considered 'almost critical'.  
> PAG: swin - количество загруженных страниц памяти из дискового кеша  
> PAG: swout - количество выгруженных страниц памяти на дисковый кеш
