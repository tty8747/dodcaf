---
author: "Sergey Chulanov"
title: "Generating self-signed certificates"
date: 2024-11-12T17:09:41+03:00
description: >-
  Generating self-signed certificates
tags:
  - tls
categories:
  - linux
  - devops
  - guide
series:
  - guide
draft: false
---

### Quick links

| Ubuntu 22.04 

## At first, let's set environment variables
```bash
DOMAIN=mongodb
SUBJ="/CN=Local trust issuer"
# format:
# SUBJ="/C=RU/ST=someobl/L=somecity/O=somecompany/CN=$DOMAIN"
```

## Get a Certificate Authority certificate
```bash
openssl genrsa -out rootCA.key 4096
openssl req -x509 -new -nodes -key rootCA.key \
  -sha256 -days 1024 -subj "$SUBJ" -out rootCA.pem
```

Now we have public and private keys: rootCA.key and rootCA.pem. Let's get information about our root Certificate Authority certificate:
```bash
cat rootCA.pem | openssl x509 -noout -text
```

Create certificate Signing Request:
```bash
openssl req -new -newkey rsa:4096 -sha256 -nodes \
  -keyout "$DOMAIN.key" -subj "/CN=$DOMAIN" -out "$DOMAIN.csr"
```

Examine the certificate Signing Request:
```bash
openssl req -text -noout -verify -in "$DOMAIN.csr"
```

And now we have request and private key: mongodb.csr and mongodb.key

Prepare requred parameters for our certificate:
> Feel free to specify additional DNS records and change another parameters like `keyUsage`
```bash
cat <<- EOF | tee ./additional.info
authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
subjectAltName = @dns_names

[dns_names]
DNS.1 = localhost
DNS.2 = *.localhost
DNS.3 = $DOMAIN
DNS.4 = *.$DOMAIN
EOF
```

Issue a certificate:
```bash
openssl x509 -req -in "$DOMAIN.csr" -CA rootCA.pem \
  -CAkey rootCA.key -CAcreateserial -out "$DOMAIN.crt" \
  -days 365 -sha256 -extfile ./additional.info
```

Examine our certificate:
```bash
cat $DOMAIN.crt | openssl x509 -noout -text
```

Make set with key and certificate:
```bash
cat $DOMAIN.crt $DOMAIN.key > $DOMAIN.pem
```

## Make der format from pem:
```bash
openssl x509 -in rootCA.pem -inform pem -out rootCA.der -outform der
```

Examine our certificate in der-format:
```bash
keytool -v -printcert -file rootCA.der
```

## Assemble and disassemle pkcs
```bash
openssl pkcs12 -export -in $DOMAIN.crt \
  -inkey $DOMAIN.key -certfile rootCA.pem \
  -out $DOMAIN.p12 -password pass:thebattleforarrakis
```

Examine p12
```bash
openssl pkcs12 -info -in $DOMAIN.p12
openssl pkcs12 -in $DOMAIN.p12 -nodes -out $DOMAIN-exported.pem
cat $DOMAIN-exported.pem | openssl x509 -noout -text
```

## Make trustStore and keyStore
```bash
keytool -importcert -trustcacerts -file rootCA.pem \
  -keystore mytruststore.jks -storepass thebattleforarrakis

keytool -v -list -keystore mytruststore.jks -storepass thebattleforarrakis
```

## Make keyStore
```bash
keytool -importkeystore -srckeystore $DOMAIN.p12 \
  -srcstoretype PKCS12 -destkeystore $DOMAIN.jks \
  -deststoretype pkcs12 -storepass thebattleforarrakis

keytool -v -list -keystore $DOMAIN.jks -storepass thebattleforarrakis
```

Verifying a certificate and key
```bash
openssl rsa -check -noout -in mongodb.key
openssl verify -CAfile rootCA.pem $DOMAIN.crt
```

Verifying of the consistency between the certificate, the request, and the key (compare md5sum):
```bash
openssl rsa -noout -modulus -in $DOMAIN.key | md5sum
openssl x509 -noout -modulus -in $DOMAIN.crt | md5sum
openssl req -noout -modulus -in $DOMAIN.csr | md5sum
```

Examples with connect and check:
```bash
openssl s_client -verify_return_error -connect google.com:443
openssl s_client -showcerts -connect  email-smtp.us-west-2.amazonaws.com:465
```

Example to examine certificate of PostgreSQL:
```bash
openssl s_client -starttls -help 
s_client: Value must be one of:
  smtp
  pop3
  imap
  ftp
  xmpp
  xmpp-server
  telnet
  irc
  mysql
  postgres
  lmtp
  nntp
  sieve
  ldap

openssl s_client -starttls postgres -connect localhost:15432 -showcerts
```

Adding ca.crt on Ubuntu node:
```bash
sudo mv ca.crt /usr/local/share/ca-certificates
sudo update-ca-certificates
```
