---
author: "Sergey Chulanov"
title: "How to use ssh and scp in Windows"
date: 2023-04-06T19:58:14+03:00
description: >-
  Quick note how to use ssh and scp in Windows
tags:
  - ssh
  - scp
categories:
  - windows
  - devops
  - ops
  - guide
series:
  - guide
draft: false
---

| Windows Server 2019

## Run Powershell as an administator and generate private and public keys
```powershell
ssh-keygen -trsa -b2048
```
## Check that files exists
```powershell
ls $env:USERPROFILE\.ssh
```
## Add public key
```powershell
type $env:USERPROFILE\.ssh\id_rsa.pub | ssh root@192.168.131.10 "cat >> .ssh/authorized_keys"
```
## Check connect without password
```powershell
ssh root@192.168.131.10
```
## Check that commands are runned
```powershell
ssh root@192.168.131.10 "cat /etc/*release"
```
## Put the file into /tmp
```powershell
scp "$env:USERPROFILE\.ssh\id_rsa.pub" "root@192.168.131.10:/tmp/"
```
## Put several files into /tmp
```powershell
scp -r "C:\tmp" "root@192.168.131.10:/tmp/123"
ssh root@192.168.131.10 "ls /tmp/123/tmp"
scp -r "root@192.168.131.10:/tmp/" "C:\tmp"
```

