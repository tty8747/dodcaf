---
author: "Sergey Chulanov"
title: "Back up and restore with MongoDB tools"
date: 2023-05-14T18:23:34+02:00
description: >-
  This tutorial describes the process for creating backups and restoring data using the utilities provided with MongoDB.
tags:
  - mongo
categories:
  - linux
  - devops
  - databases
  - nosql
  - guide
series:
  - guide
draft: false
---

### Quick links

| Ubuntu 22.04 | [Connect to MongoDB](#try-to-connect-to-mongo1.mongo-and-mongo2.mongo) | [Work with mongo](#work-with-mongo) | [Import and restore](#import-and-restore)

MongoDB backup methods from official documentation:
- [Back Up with Atlas](https://www.mongodb.com/docs/manual/core/backups/#back-up-with-atlas)
  MongoDB Atlas, the hosted MongoDB service option in the cloud.
  [Back Up with MongoDB Cloud Manager or Ops Manager:](https://www.mongodb.com/docs/manual/core/backups/#back-up-with-mms-or-ops-manager)
  * [https://www.mongodb.com/docs/manual/core/backups/#mms](https://www.mongodb.com/docs/manual/core/backups/#mms)
  * [https://www.mongodb.com/docs/manual/core/backups/#ops-manager](https://www.mongodb.com/docs/manual/core/backups/#ops-manager)
    With Ops Manager, MongoDB subscribers can install and run the same core software that powers MongoDB Cloud Manager on their own infrastructure.
- [Back Up by Copying Underlying Data Files](https://www.mongodb.com/docs/manual/core/backups/#back-up-by-copying-underlying-data-files)
  * [Back Up with Filesystem Snapshots](https://www.mongodb.com/docs/manual/core/backups/#back-up-with-filesystem-snapshots)
  * [Back Up with cp or rsync](https://www.mongodb.com/docs/manual/core/backups/#back-up-with-cp-or-rsync)
  * [Back Up with mongodump](https://www.mongodb.com/docs/manual/core/backups/#back-up-with-mongodump)

Let's consider back up with mongodump!
Run docker containers with MongoDB on board and a container for our activities with this `docker-compose.yml`:
```bash
mkdir /tmp/mongo
```
```bash
cat << EOF > /tmp/mongo/docker-compose.yml
version: '3.1'

services:

  mongo1:
    image: mongo:6
    restart: always
    environment:
      MONGO_INITDB_ROOT_USERNAME: root
      MONGO_INITDB_ROOT_PASSWORD: toor

  mongo-express1:
    image: mongo-express:1.0.0-alpha.4
    restart: always
    ports:
      - 8081:8081
    environment:
      ME_CONFIG_MONGODB_ADMINUSERNAME: root
      ME_CONFIG_MONGODB_ADMINPASSWORD: toor
      ME_CONFIG_MONGODB_URL: mongodb://root:toor@mongo1:27017/

  mongo2:
    image: mongo:6
    restart: always
    environment:
      MONGO_INITDB_ROOT_USERNAME: root
      MONGO_INITDB_ROOT_PASSWORD: toor
  
  mongo-express2:
    image: mongo-express:1.0.0-alpha.4
    restart: always
    ports:
      - 8082:8081
    environment:
      ME_CONFIG_MONGODB_ADMINUSERNAME: root
      ME_CONFIG_MONGODB_ADMINPASSWORD: toor
      ME_CONFIG_MONGODB_URL: mongodb://root:toor@mongo2:27017/

EOF
```

## Try to connect to `mongo1.mongo` and `mongo2.mongo`:
```bash
docker exec -it -w /root mongo-node-1 bash
```
```bash
apt update \
  && apt -y install wget curl gnupg2 software-properties-common apt-transport-https ca-certificates lsb-release inetutils-ping \
  && curl -fsSL https://www.mongodb.org/static/pgp/server-6.0.asc| gpg --dearmor -o /etc/apt/trusted.gpg.d/mongodb-6.gpg \
  && echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu $(lsb_release -cs)/mongodb-org/6.0 multiverse" | tee /etc/apt/sources.list.d/mongodb-org-6.0.list \
  && apt update \
  && apt -y install mongodb-mongosh mongodb-org-tools
```

```bash
mongosh --norc --host 'mongo1.mongo:27017' --username root --password toor --authenticationDatabase=admin
# or
mongosh --norc --host 'mongo2:27017' --username root --password toor --authenticationDatabase=admin
```

### Some activities:
```bash
show dbs
use myshinynewdb
db.user.insert({name:'Taro',age:10,gender:'m'})
db.createCollection('collection1')
```


[Examples to connect](https://cloud.yandex.ru/docs/managed-mongodb/operations/connect/non-sharded):
- Without ssl:
  ```bash
  mongosh --norc \
          --host '<FQDN of host 1 MongoDB>:27018,...,<FQDN of host N MongoDB>:27018' \
          --username <database user name> \
          --password <database user password> \
          <database name>
  ```
- With ssl:
  ```bash
  mongosh --norc \
          --tls \
          --tlsCAFile /home/<username>/.mongodb/root.crt \
          --host '<FQDN of host 1 MongoDB>:27018,...,<FQDN of host N MongoDB>:27018' \
          --username <database user name> \
          --password <database user password> \
          <database name>
  ```
## Work with mongo

> [How to work with collection](https://gearmobile.github.io/mongodb/mongodb-collections/)

### Create a collection
```bash
use animals
animals> db.createCollection('cats')
animals> db.createCollection('dogs')
animals> show collections
  cats
  dogs
```
### Rename the collection
```bash
animals> db.createCollection('bird')
animals> show collections
  bird
  ...
animals> db.bird.renameCollection('birds')
show collections
  birds
  ...
```
### Delete the collection
```bash
animals> db.birds.drop()
```

### Create a collection - automatic method
```bash
animals> db.insects.insert({ name: 'cockroach' })
animals> db.insects.find()
  [ { _id: ObjectId("64613df28befa5fc404ef60c"), name: 'cockroach' } ]
```

### Move a database
```bash
db.copyDatabase("db_to_rename","db_renamed","localhost")
use db_to_rename
db.db_to_rename.drop()
```
```bash
mongodump -d old_db_name -o mongodump/
mongorestore -d new_db_name mongodump/old_db_name
db.old_db_name.drop()
```

## Import and restore:

> [Back up and restore a MongoDB database](https://www.digitalocean.com/community/tutorials/how-to-back-up-restore-and-migrate-a-mongodb-database-on-ubuntu-20-04)

It is convenient to work with `JSON`, but it does not retain all data formats. This means that you can get inconsistent data compared to `BSON`. A better way to back up and restore data is to use binary `BSON`.

### Back up database

It is a good approach to shut down all connected databases with our `MongoDB`, if they exist. In my case, I shut down `Neo4j` and scaled the root component to 0 in my `Kubernetes` cluster.
> For all dbs we can remove `--db animals`
```bash
mkdir /var/backups/mongobackups
```
```bash
mongodump --host 'mongo1.mongo:27017' --username root --password toor --authenticationDatabase admin --db animals --out /var/backups/mongobackups/`date +"%m-%d-%y"`
  2023-05-14T20:23:05.758+0000  writing animals.insects to /var/backups/mongobackups/05-14-23/animals/insects.bson
  2023-05-14T20:23:05.759+0000  writing animals.dogs to /var/backups/mongobackups/05-14-23/animals/dogs.bson
  2023-05-14T20:23:05.760+0000  done dumping animals.insects (1 document)
  2023-05-14T20:23:05.760+0000  done dumping animals.dogs (0 documents)
  2023-05-14T20:23:05.762+0000  writing animals.cats to /var/backups/mongobackups/05-14-23/animals/cats.bson
  2023-05-14T20:23:05.763+0000  done dumping animals.cats (0 documents)
```

### Restore database

We'll start by indicating the database name with the `--nsInclude` argument. We'll be using `newdb.*` to restore all collections. To restore just one collection, like `restaurants`, use `newdb.restaurants` instead.  
Using --drop, we’ll make sure that the target database is first dropped so that the backup is restored in a clean database.

```bash
mongorestore --host 'mongo2.mongo:27017' --username root --password toor --authenticationDatabase=admin --db animals --drop /var/backups/mongobackups/05-14-23/animals/
  ...
  2023-05-14T20:29:12.061+0000  finished restoring animals.insects (1 document, 0 failures)
  2023-05-14T20:29:12.065+0000  finished restoring animals.cats (0 documents, 0 failures)
  2023-05-14T20:29:12.070+0000  finished restoring animals.dogs (0 documents, 0 failures)
  2023-05-14T20:29:12.070+0000  no indexes to restore for collection animals.dogs
  2023-05-14T20:29:12.070+0000  no indexes to restore for collection animals.insects
  2023-05-14T20:29:12.070+0000  no indexes to restore for collection animals.cats
  2023-05-14T20:29:12.070+0000  1 document(s) restored successfully. 0 document(s) failed to restore.
```

## Replication and Replica Sets of MongoDB

> https://www.mongodb.com/docs/manual/faq/replica-sets/

Usefull links:
- [How to install MongoDB](https://www.digitalocean.com/community/tutorials/how-to-install-mongodb-on-ubuntu-20-04)
- [How to install Compass](https://www.mongodb.com/docs/compass/master/install/)
