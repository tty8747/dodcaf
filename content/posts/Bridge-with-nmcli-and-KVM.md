---
author: "Sergey Chulanov"
title: "Bridge with nmcli and KVM"
date: 2023-08-13T11:48:59+03:00
description: >-
  Bridge with nmcli and KVM
tags:
  - bridge
  - nmcli
categories:
  - linux
  - devops
  - ops
  - guide
series:
  - guide
draft: false
---

### Quick links

| Ubuntu 22.04

Get current network config:
```bash
nmcli con show
nmcli connection show --active
```

> We musn't use wireless interface?
> I've spend a lot of time to find solution, but wireless bridge always doesn't work.  
> This approach also doesn't work:  
> ```bash
> # To bridge wifi interface you may use iw tool to enable 4addr likewise:
> # iw dev <wifiInterface> set 4addr on
> # brctl addif <bridgename> <wifiInterface>
> can't add <wifiInterface> to bridge <bridgename>: Operation not supported
> # iw dev <wifiInterface> set 4addr on
> # brctl addif <bridgename> <wifiInterface>
> Now it should work. You can show bridges using:
> # brctl show
> ```
> Hmm: [link](https://serverfault.com/questions/152363/bridging-wlan0-to-eth0)
> Hmm:[link](https://forum.proxmox.com/threads/wlan-doesnt-stay-in-bridge.49978/)

Create a bridge, named `br0`, `enp1s0` is my eth0:
```bash
sudo nmcli con add ifname br0 type bridge con-name br0
sudo nmcli con add type bridge-slave ifname enp1s0 master br0
nmcli connection show
```

Disable STP:
```bash
sudo nmcli con modify br0 bridge.stp no
nmcli con show
nmcli -f bridge con show br0
```

Turn on bridge interface:
```bash
sudo nmcli con down "Wired connection 1"
sudo nmcli con up br0
nmcli con show
```

Use `ip` to view settings:
```bash
ip -c a s br0
	3: br0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default qlen 1000
	    link/ether 86:af:40:4f:75:20 brd ff:ff:ff:ff:ff:ff
	    inet 192.168.137.218/24 brd 192.168.137.255 scope global dynamic noprefixroute br0
	       valid_lft 81402sec preferred_lft 81402sec
	    inet6 fd81:23c:1c44:1:68c8:5a6f:724f:e9bf/64 scope global temporary dynamic
	       valid_lft 599807sec preferred_lft 81388sec
	    inet6 fd81:23c:1c44:1:4385:e183:63b7:c44b/64 scope global dynamic mngtmpaddr noprefixroute
	       valid_lft 604801sec preferred_lft 86401sec
	    inet6 fe80::de15:9f63:99e2:5601/64 scope link noprefixroute
	       valid_lft forever preferred_lft forever
```

## Use `br0` with KVM:
```bash
cat /tmp/br0.xml
<network>
  <name>br0</name>
  <forward mode="bridge"/>
  <bridge name="br0" />
</network>
```

Run virsh command as follows:
```bash
virsh net-define /tmp/br0.xml
virsh net-start br0
virsh net-autostart br0
virsh net-list --all
	 Name      State    Autostart   Persistent
	--------------------------------------------
	 br0       active   yes         yes
	 default   active   yes         yes
```

Better explanation[here](https://www.cyberciti.biz/faq/how-to-add-network-bridge-with-nmcli-networkmanager-on-linux/)
