---
title: "Задание умолчаний, для установки deb-пакета"
author: "Sergey Chulanov"
date: 2022-01-14T23:02:45+03:00
description: >-
  Задание умолчаний, для установки deb-пакета
tags:
  - Docker
  - deb
  - debconf
categories:
  - linux
series:
  - guide
draft: false
---

# Задание умолчаний, для установки deb-пакета:

- `debconf-get-selections` - Команда выводит содержимое базы данных debconf
- `debconf-set-selections` - Команда может быть использована для автоматической настройки ответов из базы данных `debconf` или для изменения имеющихся ответов.
- `debconf-communicate` - Команда позволяет обращаться к debconf из командной строки.
- `debconf-show` - Команда позволяет выполнить запрос к базе данных debconf различными способами.

## Подготовка:

```bash
apt update -y
apt install debconf-utils
```

## Установим целевой пакет и ответим на вопросы. При установке видно, что выбираем для пакета `tzdata`:

```bash
apt install libgtk-3-0

Configuring tzdata
------------------

Please select the geographic area in which you live. Subsequent configuration questions will narrow
this down by presenting a list of cities, representing the time zones in which they are located.

  1. Africa      4. Australia     7. Atlantic Ocean  10. Pacific Ocean       13. None of the above
  2. America     5. Arctic Ocean  8. Europe          11. System V timezones
  3. Antarctica  6. Asia          9. Indian Ocean    12. US
Geographic area: 8

Please select the city or region corresponding to your time zone.

  1. Amsterdam   12. Büsingen    23. Kiev        34. Moscow      45. Saratov     56. Vatican
  2. Andorra     13. Chisinau     24. Kirov       35. Nicosia     46. Simferopol  57. Vienna
  3. Astrakhan   14. Copenhagen   25. Lisbon      36. Oslo        47. Skopje      58. Vilnius
  4. Athens      15. Dublin       26. Ljubljana   37. Paris       48. Sofia       59. Volgograd
  5. Belfast     16. Gibraltar    27. London      38. Podgorica   49. Stockholm   60. Warsaw
  6. Belgrade    17. Guernsey     28. Luxembourg  39. Prague      50. Tallinn     61. Zagreb
  7. Berlin      18. Helsinki     29. Madrid      40. Riga        51. Tirane      62. Zaporozhye
  8. Bratislava  19. Isle of Man  30. Malta       41. Rome        52. Tiraspol    63. Zurich
  9. Brussels    20. Istanbul     31. Mariehamn   42. Samara      53. Ulyanovsk
  10. Bucharest  21. Jersey       32. Minsk       43. San Marino  54. Uzhgorod
  11. Budapest   22. Kaliningrad  33. Monaco      44. Sarajevo    55. Vaduz
Time zone: 34
```

## Отсортируем наши ответы, при установке libgtk-3-0 было выбрано `Moscow` и `Europe`:

```bash
debconf-get-selections | grep Moscow -C1
# Time zone:
# Choices: Amsterdam, Andorra, Astrakhan, Athens, Belfast, Belgrade, Berlin, Bratislava, Brussels, Bucharest, Budapest, Büsingen, Chisinau, Copenhagen, Dublin, Gibraltar, Guernsey, Helsinki, Isle of Man, Istanbul, Jersey, Kaliningrad, Kiev, Kirov, Lisbon, Ljubljana, London, Luxembourg, Madrid, Malta, Mariehamn, Minsk, Monaco, Moscow, Nicosia, Oslo, Paris, Podgorica, Prague, Riga, Rome, Samara, San Marino, Sarajevo, Saratov, Simferopol, Skopje, Sofia, Stockholm, Tallinn, Tirane, Tiraspol, Ulyanovsk, Uzhgorod, Vaduz, Vatican, Vienna, Vilnius, Volgograd, Warsaw, Zagreb, Zaporozhye, Zurich
tzdata	tzdata/Zones/Europe	select	Moscow
```

Из вывода команды тоже видно, что интерактивно выбирать значения пришлось для пакета `tzdata`

## Посмотрим что на данный момент выбрано `Moscow` и `Europe`:

```bash
debconf-show tzdata | grep "*"
	* tzdata/Zones/Europe: Moscow
	* tzdata/Areas: Europe
```

## Теперь получим строки, для установки:

> Эта конструкция `grep -vE select$'\t'$` позволяет отфильтровать слово `select` с табом в конце строки (`select<tab>$`, где <tab> это $'\t')

> Здесь между значениями установлена табуляция, в дальнейшем, её нужно заменить на пробелы

```bash
debconf-get-selections | grep tzdata | grep -vE select$'\t'$
tzdata	tzdata/Zones/Etc	select	UTC
tzdata	tzdata/Zones/Europe	select	Moscow
tzdata	tzdata/Areas	select	Europe
```

## Для примера сменим один параметр или можно использовать для установки параметра при написании `Dockerfile`:

```bash
echo "tzdata tzdata/Zones/Europe select Monaco" | debconf-set-selections
```

> Так же `debconf-set-selections` принимает файл со значениями.

## Проверим, что изменения произошли:

```bash
debconf-show tzdata | grep "*"
	* tzdata/Zones/Europe: Monaco
	* tzdata/Areas: Europe
```

При установке нужно указать `DEBIAN_FRONTEND="noninteractive"`, а для `Dockerfile` можно установить `ARG DEBIAN_FRONTEND=noninteractive`:

```bash
DEBIAN_FRONTEND="noninteractive" apt install -y libgtk-3-0
```

## Базовый пример `Dockerfile`:

```bash
FROM ubuntu:20.04

ARG DEBIAN_FRONTEND=noninteractive
ENV LANG=en_US.UTF-8 \
    LC_CTYPE=ru_RU.UTF-8 \
    LC_NUMERIC=ru_RU.UTF-8 \
    LC_TIME=ru_RU.UTF-8 \
    LC_COLLATE=ru_RU.UTF-8 \
    LC_MONETARY=ru_RU.UTF-8 \
    LC_MESSAGES=en_US.UTF-8 \
    LC_PAPER=ru_RU.UTF-8 \
    LC_NAME=ru_RU.UTF-8 \
    LC_ADDRESS=ru_RU.UTF-8 \
    LC_TELEPHONE=ru_RU.UTF-8 \
    LC_MEASUREMENT=ru_RU.UTF-8 \
    LC_IDENTIFICATION=ru_RU.UTF-8

RUN apt-get -qq update \
    && echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true | debconf-set-selections \
    && apt -qq install --yes debconf-utils locales \
    && locale-gen en_US.UTF-8 ru_RU.UTF-8

ENTRYPOINT ["/bin/bash"]
```

Скрипт билда с запуском:
```bash
#!/bin/bash

docker build -t test:v0.1 . && docker run --rm --name test -it test:v0.1
```
