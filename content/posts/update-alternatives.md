---
author: "Sergey Chulanov"
title: "Update Alternatives"
date: 2022-09-10T22:28:42+03:00
description: >-
  Работа с update-alternatives
tags:
  - ubuntu
  - update-alternatives
categories:
  - linux
series:
  - guide
draft: false
---

| Ubuntu 22.04 | update-alternatives

Используйте приведённую команду, для просмотра всех вариантов `update-alternatives`
```bash
update-alternatives --get-selections
awk                            auto     /usr/bin/mawk
builtins.7.gz                  auto     /usr/share/man/man7/bash-builtins.7.gz
pager                          auto     /bin/more
rmt                            auto     /usr/sbin/rmt-tar
which                          auto     /usr/bin/which.debianutils
```

# update-alternatives на примере `clang`.

При установке на данный момент устанавливается 14 версия по-умолчанию:
```bash
$ clang -v 2>&1 | head -1   
Ubuntu clang version 14.0.0-1ubuntu1
```

Допустим, понадобилась версия 13, установим её в систему:
```bash
$ sudo apt install clang-13
```

Посмотрим альтернативы для `c++`:
```bash
$ update-alternatives --list c++
/usr/bin/clang++
/usr/bin/g++

$ update-alternatives --display c++
c++ - ручной режим
  link best version is /usr/bin/g++
  ссылка сейчас указывает на /usr/bin/clang++
  link c++ is /usr/bin/c++
  slave c++.1.gz is /usr/share/man/man1/c++.1.gz
/usr/bin/clang++ — приоритет 10
/usr/bin/g++ — приоритет 20
  подчинённый c++.1.gz: /usr/share/man/man1/g++.1.gz
```
Определим где находятся бинарники:
```bash
$ which clang
/usr/bin/clang

$ ls -l /usr/bin/clang                                
lrwxrwxrwx 1 root root 24 апр  1 23:51 /usr/bin/clang -> ../lib/llvm-14/bin/clang

$ ls /usr/lib/llvm-13/bin/clang /usr/lib/llvm-14/bin/clang
/usr/lib/llvm-13/bin/clang  /usr/lib/llvm-14/bin/clang
```

> Добавить и удалить:
> `$ sudo update-alternatives --install [ссылка]/usr/bin/clang [название]c++ [путь к бинарнику]/usr/lib/llvm-13/bin/clang [приоритет]3`
> `$ sudo update-alternatives --remove [название]c++ [путь к бинарнику]/usr/lib/llvm-13/bin/clang`

Добавим альтернативу для `c++`:
```bash
$ sudo update-alternatives --install /usr/bin/clang c++ /usr/lib/llvm-13/bin/clang 3
```

Выбираем 13 версию в интерактивном режиме:
```bash
$ sudo update-alternatives --config c++

$ clang -v 2>&1 | head -1
Ubuntu clang version 13.0.1-2ubuntu2.1
```

Выбираем в неинтерактивном режиме:
```bash
sudo update-alternatives --set c++ /usr/bin/clang++

$ clang -v 2>&1 | head -1                            
Ubuntu clang version 14.0.0-1ubuntu1
```
