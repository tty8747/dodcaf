---
author: "Sergey Chulanov"
title: "Ingress on Minikube"
date: 2023-02-24T16:19:25+03:00
description: >-
  some description
tags:
  - k8s
  - ingress
  - minikube
  - nginx
categories:
  - devops
  - guide
series:
  - guide
draft: false
---

# Ingress on minikube

First off, remove our minikube:
```bash
minikube delete --profile minikube
```

And install minikube:
```bash
# https://minikube.sigs.k8s.io/docs/faq/#how-can-i-access-a-minikube-cluster-from-a-remote-network
minikube start --nodes 3 --profile=minikube --driver=docker --listen-address=0.0.0.0
minikube addons enable metrics-server
```

Get container id of minikube and check ports (`--listen-address`):
```bash
docker ps --filter name=minikube -q
	9bafec0763c9
	6bec57ee7992
	6090eadfd27f
docker port 9bafec0763c9
docker port 6bec57ee7992
docker port 6090eadfd27f
	22/tcp -> 0.0.0.0:32849
	2376/tcp -> 0.0.0.0:32848
	5000/tcp -> 0.0.0.0:32847
	8443/tcp -> 0.0.0.0:32846
	32443/tcp -> 0.0.0.0:32845
```

Enable minikube in our installation:
```bash
minikube addons enable ingress
```

Check ingress pods:
```bash
kubectl get pods -n ingress-nginx
	NAME                                       READY   STATUS      RESTARTS   AGE
	ingress-nginx-admission-create-w7vn8       0/1     Completed   0          21s
	ingress-nginx-admission-patch-4chvd        0/1     Completed   0          21s
	ingress-nginx-controller-77669ff58-x9vkk   0/1     Running     0          21s
```

Realize this schema:
```bash
User --> Path --v1-> app v1.0
              \-v2-> app v2.0
```

Create 2 deployments:
```bash
kubectl create deployment appv1 --image=gcr.io/google-samples/hello-app:1.0 --dry-run=client -o yaml | kubectl apply -f-
kubectl create deployment appv2 --image=gcr.io/google-samples/hello-app:2.0 --dry-run=client -o yaml | kubectl apply -f-
kubectl scale deployment appv1 --replicas 2
kubectl scale deployment appv2 --replicas 4
```

Create 2 services:
```bash
# --port= is a port of our service,
# --target-port= - where our app is running inside the pod
kubectl expose deployment appv1 --name=appv1-svc --type=NodePort --port=8080 --dry-run=client -o yaml | kubectl apply -f-
kubectl expose deployment appv2 --name=appv2-svc --type=ClusterIP --port=9090 --target-port=8080 --dry-run=client -o yaml | kubectl apply -f-
```

Check:
```bash
kubectl get pods -o wide
	NAME                     READY   STATUS    RESTARTS   AGE   IP           NODE           NOMINATED NODE   READINESS GATES
	appv1-7f688c5c7c-k2478   1/1     Running   0          48s   10.244.2.6   minikube-m03   <none>           <none>
	appv1-7f688c5c7c-sj24n   1/1     Running   0          23m   10.244.1.3   minikube-m02   <none>           <none>
	appv2-9446dbfc7-2p2sj    1/1     Running   0          17m   10.244.0.6   minikube       <none>           <none>
	appv2-9446dbfc7-92z6f    1/1     Running   0          23m   10.244.2.4   minikube-m03   <none>           <none>
	appv2-9446dbfc7-c9hsz    1/1     Running   0          17m   10.244.1.4   minikube-m02   <none>           <none>
	appv2-9446dbfc7-kq8sh    1/1     Running   0          17m   10.244.2.5   minikube-m03   <none>           <none>

kubectl get services -o wide
	NAME         TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)          AGE     SELECTOR
	appv1        NodePort    10.108.172.125   <none>        8080:32516/TCP   5m27s   app=appv1
	appv2        ClusterIP   10.103.61.88     <none>        9090/TCP         47s     app=appv2
```

> It's not recommended to use Node port for productuon environments. (Need to find a proof!!!)

Check with container:
```bash
# check pods
kubectl --namespace default run alpine --image alpine:latest \
	--rm --stdin --tty \
	-- sh -c "apk add curl; curl -D - http://10.244.0.6:8080; echo; curl -D - http://10.244.1.3:8080"
# check services
kubectl run alpine --image alpine --tty --stdin --rm -- sh -c "apk add curl; sh"
curl http://10.108.172.125:8080
	Hello, world!
	Version: 1.0.0
	Hostname: appv1-7f688c5c7c-sj24n
curl http://10.103.61.88:9090
	Hello, world!
	Version: 2.0.0
	Hostname: appv2-9446dbfc7-92z6f
```

Now, let's create ingress:
```bash
kubectl create ingress ingress-nginx --rule=check.my.awesome.ingress.com/v1=appv1-svc:8080 --rule=check.my.awesome.ingress.com/v2=appv2-svc:9090 --annotation='nginx.ingress.kubernetes.io/rewrite-target=/$1' --dry-run=client -o yaml | kubectl apply -f-
```

Get cluster ip, and put it into `/etc/hosts`:
```bash
minikube ip
	192.168.58.2
cat /etc/hosts | grep "192.168.58.2"
	192.168.58.2 check.my.awesome.ingress.com
```

Let's ensure that our ingress is working:
```bash
curl -D - "http://check.my.awesome.ingress.com/v1"
	HTTP/1.1 200 OK
	Date: Fri, 24 Feb 2023 11:53:32 GMT
	Content-Type: text/plain; charset=utf-8
	Content-Length: 62
	Connection: keep-alive

	Hello, world!
	Version: 1.0.0
	Hostname: appv1-7f688c5c7c-k2478

curl -D - "http://check.my.awesome.ingress.com/v2"
	HTTP/1.1 200 OK
	Date: Fri, 24 Feb 2023 11:53:35 GMT
	Content-Type: text/plain; charset=utf-8
	Content-Length: 61
	Connection: keep-alive

	Hello, world!
	Version: 2.0.0
	Hostname: appv2-9446dbfc7-kq8sh
```
