
---
author: "Sergey Chulanov"
title: "Vnc Server on Ubuntu 20.04."
date: 2021-09-22T14:03:53+03:00
description: >-
  Установка и настрока VNC-server
tags:
  - vnc
categories:
  - linux
series:
  - guide
draft: false
---

Создадим пользователя:

```bash
$ sudo useradd -s /bin/bash -m -G sudo user
```

Обновим пакеты и установим необходимые пакеты для работы графики.
```bash
$ sudo apt update && sudo apt install xfce4 xfce4-goodies && sudo apt install tigervnc-standalone-server
```

Сгенерируем пароль и определим, что не собираемся использовать только для просмтра экрана:
```bash
$ vncpasswd
...
Would you like to enter a view-only password (y/n)? n
```

Создадим конфиг:
```bash
$ cat <<- EOF | tee --append ~/.vnc/xstartup
#!/bin/sh
unset SESSION_MANAGER
unset DBUS_SESSION_BUS_ADDRESS
exec startxfce4
geometry=1024x768
dpi=96
EOF
```

> Полезные команды:  
> vncserver -h  
> vncserver -list  
> vncserver -kill :1

Теперь создадим systemd daemon, тут нужно указать пользователя в поле `User=`:

```bash
$ cat <<- EOF | sudo tee --append /etc/systemd/system/vncserver@.service
[Unit]
Description=Remote desktop service (VNC)
After=syslog.target network.target

[Service]
Type=simple
User=user
PAMName=login
PIDFile=/home/%u/.vnc/%H%i.pid
ExecStartPre=/bin/sh -c '/usr/bin/vncserver -kill :%i > /dev/null 2>&1 || :'
ExecStart=/usr/bin/vncserver :%i -geometry 1024x768 -alwaysshared -fg -localhost no
ExecStop=/usr/bin/vncserver -kill :%i

[Install]
WantedBy=multi-user.target
EOF
```

Ну а теперь перечитаваем конфиги `systemd`, включаем `vncserver` в автозагрузку,  запускаем его и запрашиваем статус:

```bash
$ sudo systemctl daemon-reload && sudo systemctl enable vncserver@1.service && sudo systemctl start vncserver@1.service && sudo systemctl status vncserver@1.service
```

Запущенный процесс должен висеть на 590x-порту, проверим:

```bash
$ sudo lsof -i -P -n | grep 590
Xtigervnc 19105            user    7u  IPv4  62185      0t0  TCP *:5901 (LISTEN)
Xtigervnc 19105            user    8u  IPv6  62186      0t0  TCP *:5901 (LISTEN)
```

Теперь можно пробовать подключиться. Я подключаюсь с помощью `Remmina` с модулем `Модуль VNC Remmina`, где в поле `Сервер` нужно указать `vnc.dodcaf.ru:5901` и указать пароль.
