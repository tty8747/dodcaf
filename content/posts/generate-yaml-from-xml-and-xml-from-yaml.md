---
author: "Sergey Chulanov"
title: "Generate yaml from xml and xml from yaml"
date: 2023-02-13T17:36:37+03:00
description: >-
  Generate yaml from xml and xml from yaml
tags:
  - yaml
  - xml
categories:
  - devops
series:
  - guide
draft: false
---

## First off we should get schema for out `pom.xml`. It is on the head of `pom.xml`:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
```

Get it:
```bash
wget http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd
```

## Secondly, we need to install `xml2yaml`:
- Debian,  Ubuntu,  Kali Linux,  Raspbian:
  ```bash
  apt-get install libxml-compile-perl
  xml2yaml help 
  ERROR: no schema's specified
  ```

## Next, there are exmaples of commands:
```bash
xml2yaml xml-file schema-file(s)  >yaml-file
xml2yaml -x xml-file -s schema-files -o yaml-file
# for our cas `maven-4.0.0.xsd` was downloaded on the first step
xml2yaml -x pom.xml -s maven-4.0.0.xsd -o 1.yml
```
> There is a useful [link](https://linux.die.net/man/1/xml2yaml)

## After that, let's install yq:
```bash
sudo add-apt-repository ppa:rmescandon/yq
sudo apt update
sudo apt install yq
```

## Then, try than everything is ok:
```bash
yq -v
```

## Finally, generate `xml` from `yaml`:
> This we can't get the same xml-file.
```bash
yq  --xml-attribute-prefix @   \
    --xml-content-name '#text' \
    --input-format yaml        \
    --output-format xml        \
    1.yml > 1.xml
```
