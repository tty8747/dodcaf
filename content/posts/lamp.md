---
author: "Sergey Chulanov"
title: "Обновление интернет-магазина shopware."
date: 2021-10-09T23:37:03+03:00
description: >-
  Переезд shopware 4 со старого хостинга, где отстутствует поддержка php7.4. Обновление на shopware 5.
tags:
  - lamp
  - apache
  - mariadb
categories:
  - linux
series:
  - guide
draft: false
---

| Ubuntu 20.04 | php5.6 | php7.4 | apache24 | mariadb 10.6 |

### Переезд shopware 4 со старого хостинга, где отстутствует поддержка php7.4. Обновление на shopware 5.

Так как на старом хостинге нет поддержки php7.4, то придётся перенести базу данных и файлы на ноду, где установлена php5.6, а затем обновиться до php7.4.

Я уже подготовил данные:
  - скачал файлы со старого хостинга
  - сделал дамп базы MySQL через phpmyadmin.

Первым делом нужно просто перенести текущую версию интернет-магазина shopware 4. Для этого подготовим сервер.

Apache24:
```bash
sudo apt update
sudo apt install apache2
curl "http://localhost"
```

MariaDB:
```bash
sudo apt install software-properties-common
sudo apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xF1656F24C74CD1D8
sudo add-apt-repository 'deb [arch=amd64,arm64,ppc64el] http://mirror.mephi.ru/mariadb/repo/10.6/ubuntu focal main'
sudo apt update
sudo apt install mariadb-server
sudo mysql_secure_installation
sudo mysql -uroot

Создать базу данных:
MariaDB [(none)]> CREATE DATABASE shopware4;

Создать пользователя только для входа с localhost:
MariaDB [(none)]> CREATE USER 'shop'@'localhost' IDENTIFIED BY 'Qaz123';

Не сработало, проверить ->
Дать полный доступ пользователю к базе данных:
MariaDB [(none)]> GRANT ALL PRIVILEGES ON shopware4.* TO 'shop'@'localhost';

Перезагрузить привилегии:
MariaDB [(none)]> FLUSH PRIVILEGES;
MariaDB [(none)]> EXIT;

Дать права на всё (*)
MariaDB [(none)]> GRANT ALL PRIVILEGES ON * . * TO 'shop'@'localhost';

Загрузим дамп базы данных:
pass=Qaz123
mysql shopware4 -ushop -p$pass < ~/DB1754314.sql

Убедимся, что в базе теперь есть куча таблиц:
mysql -ushop -p$pass
MariaDB [(none)]> show databases;
MariaDB [(none)]> use shopware4;
MariaDB [shopware4]> show tables;
```

> Сделать бэкап базы и файлов:  
> ```bash
> mysqldump -v -ushop -p$pass shopware4 > ~/shopwaredb5.1.6.sql
> tar -zcvf ~/shopware5.1.6.tgz /var/www/html/shopware
> ```

PHP 5.6:

```bash
sudo add-apt-repository ppa:ondrej/php
sudo apt install php5.6 php5.6-cli php5.6-fpm php5.6-curl \
                 php5.6-json php5.6-zip php5.6-gd php5.6-xml \
                 php5.6-mbstring php5.6-opcache php5.6-mysql \
                 php5.6-intl unzip libapache2-mod-php5.6 php5.6-common \
                 php5.6-xmlrpc php5.6-mysql
sudo a2enmod rewrite
sudo systemctl restart apache2
```

PHP 7.4:

```bash
sudo add-apt-repository ppa:ondrej/php
sudo apt install php7.4 php7.4-cli php7.4-fpm php7.4-curl \
                 php7.4-json php7.4-zip php7.4-gd php7.4-xml \
                 php7.4-mbstring php7.4-opcache php7.4-mysql \
                 php7.4-intl unzip libapache2-mod-php7.4 php7.4-common \
                 php7.4-xmlrpc php7.4-mysql
sudo a2enmod rewrite
sudo systemctl restart apache2
```

Настраиваем согласно [рекомендациям для shopware](https://developers.shopware.com/sysadmins-guide/system-requirements/):


```bash
cat /etc/php/5.6/apache2/php.ini

# PHP OPcache
opcache.memory_consumption=128
opcache.interned_strings_buffer=8
opcache.max_accelerated_files=4000
opcache.revalidate_freq=60
opcache.fast_shutdown=1
opcache.enable_cli=1

# PHP settings
file_uploads = On
allow_url_fopen = On
short_open_tag = On
memory_limit = 256M
upload_max_filesize = 100M
max_execution_time = 360
date.timezone = Europe/Berlin
```

```bash
cat /etc/php/7.4/apache2/php.ini

# PHP OPcache
opcache.memory_consumption=128
opcache.interned_strings_buffer=8
opcache.max_accelerated_files=4000
opcache.revalidate_freq=60
opcache.fast_shutdown=1
opcache.enable_cli=1

# PHP settings
file_uploads = On
allow_url_fopen = On
short_open_tag = On
memory_limit = 256M
upload_max_filesize = 100M
max_execution_time = 360
date.timezone = Europe/Berlin
```

Optional MySQL settings
MySQL variable group_concat_max_len should be increased to 2048 (default: 1024)
innodb_buffer_pool_size should be at least as large as the whole database
query_cache_size should normally be between 100 - 200MB

```bash
cat /etc/mysql/mariadb.conf.d/150-custom-shopware.cnf
[mysqld]
group_concat_max_len = 2048M
innodb_buffer_pool_size = 2G
query_cache_limit = 10M
query_cache_size = 100M
```
Для проверки установленных значений:
mysql> SHOW VARIABLES LIKE 'have_query_cache'

| Variable_name | Value |
| ------------- | ----- |
| have_query_cache | **YES**|

`YES` указывает на то, что кеш включён.


Для мониторинга:
mysql> show status like '%Qcache%';

| Variable_name | Value |
| ------------- | ----- |
| Qcache_free_blocks | 165 |
| Qcache_free_memory | 3893664 |
| Qcache_hits | 4654886 |
| Qcache_inserts | 352314 |
| Qcache_lowmem_prunes | 301 |
| Qcache_not_cached | 66691 |
| Qcache_queries_in_cache | 147 |
| Qcache_total_blocks | 469 |

Значение `Qcache_hits` должно быть очень большим.

Для очистки кеша:
FLUSH QUERY CACHE


/var/www/html/phpinfo.php
```php
<?php
  phpinfo();
?>
```
curl http://localhost/phpinfo.php
http://k8s/phpinfo.php

> [HINT!](https://stackoverflow.com/questions/19737525/find-type-f-exec-chmod-644)
> Piping to xargs is a dirty way of doing that which can be done inside of find.
>
> find . -type d -exec chmod 0755 {} \;
> find . -type f -exec chmod 0644 {} \;
> You can be even more controlling with other options, such as:
>
> find . -type d -user harry -exec chown daisy {} \;

> Переключение версии php через CLI:
> sudo update-alternatives --config php
> или sudo update-alternatives --set php /usr/bin/php7.4
> sudo update-alternatives --set php /usr/bin/php5.6

> Переключение с помощью apache c 7.4 на 5.6:
> sudo a2dismod php7.4
> sudo a2enmod php5.6
> sudo service apache2 restart

эти команды по сути удаляют и создают симлинки. Убедиться можно заглянув в каталог mods*:
```bash
ls -l /etc/apache2/mods-*/*php*
-rw-r--r-- 1 root root 867 сен 23 21:57 /etc/apache2/mods-available/php5.6.conf
-rw-r--r-- 1 root root 102 сен 23 21:57 /etc/apache2/mods-available/php5.6.load
lrwxrwxrwx 1 root root  29 окт  5 17:23 /etc/apache2/mods-enabled/php5.6.conf -> ../mods-available/php5.6.conf
lrwxrwxrwx 1 root root  29 окт  5 17:23 /etc/apache2/mods-enabled/php5.6.load -> ../mods-available/php5.6.load
```

распаковываем данные в `/var/www/html/shopware/` и модифицируем файл:
```bash
cat /var/www/html/shopware/config.php
<?php return array (
  'db' =>
  array (
    'username' => 'shop',
    'password' => 'Qaz123',
    'host' => 'localhost',
    'port' => '3306',
    'dbname' => 'shopware4',
  ),
);
```

Решаем проблему с правами:
```bash
sudo find /var/www/html/shopware/ -type d -exec chmod 0755 {} \;
sudo find /var/www/html/shopware/ -type f -exec chmod 0644 {} \;
# sudo rm -rf /var/www/html/shopware/cache/*
# sudo rm -rf /var/www/html/shopware/logs/*
sudo chown -Rv www-data:www-data /var/www/html/shopware
```


Сконфигурируем virtualhost apache:

```bash
cat /etc/apache2/sites-available/shopware.conf
<VirtualHost *:80>
     ServerAdmin admin@example.com
     DocumentRoot /var/www/html/shopware
     ServerName example.com

     <Directory /var/www/html/shopware/>
          Options FollowSymlinks
          AllowOverride All
          Require all granted
     </Directory>

     ErrorLog ${APACHE_LOG_DIR}/shopware-error.log
     CustomLog ${APACHE_LOG_DIR}/shopware-access.log combined

</VirtualHost>

sudo a2ensite shopware.conf
sudo a2dissite 000-default.conf
sudo a2enmod rewrite
sudo systemctl reload apache2.service
```

> При входе будет выкидывать на оригинальный сайт, чтобы обойти это можно указать в файле `/etc/hosts` переопределив значение dns-имени : ip-адресу нового сервера. Затем зайти в админку и поменять внутри CMS установить доменное имя на нужное. (для 4й версии shopware alias /backend).


Теперь можно приступать к обновлению. Скачать их можно [тут](https://www.shopware.com/en/changelog-sw5/#5-7-3). [Официальный гайд по обновлению](https://docs.shopware.com/en/shopware-5-en/update-guides/updating-shopware?category=shopware-5-en/update-guides) Секция `Update via shell`.

> Я проводил обновление с 4й версии shopware на 5. Не получалось обновиться с помощью веб-интерфейса, из-за того что автопдейтер предлагал обновиться сразу на версию с php7.4+. Чтобы решить эту проблему я обновил на переходную версию (указана поддержка php5.6+ и php7.4+) shopware 5.5.7, затем переключил c php5.6+ на php7.4+ и стал обновлять дальше.

Перед обновлением я отключил все плагины и удалил устаревшие.  
Мануал по командам [здесь](https://docs.shopware.com/en/shopware-5-en/tutorials-and-faq/shopware-cli)

```bash
# список всех плагинов:
sudo -u www-data php bin/console sw:plugin:list
# удалить плагин:
sudo -u www-data php bin/console sw:plugin:uninstall SwagPaymentPaypal
```

Опишу здесь порядок обновлений до версии 5.5.7:

```bash
sudo -u www-data bash
cd /tmp
wget https://www.shopware.com/en/Download/redirect/version/sw5/file/update_5.5.7_5a8d8cb57f6d70de39231060532ac81e4831d40d.zip
unzip -d /var/www/html/shopware /tmp/update_5.5.7_5a8d8cb57f6d70de39231060532ac81e4831d40d.zip
cd /var/www/html/shopware
php recovery/update/index.php
rm -rf /var/www/html/shopware/update-assets
exit
sudo a2dismod php5.6
sudo a2enmod php7.4
sudo systemctl restart apache2
```

> Если возникают ошибки, нужно не забывать заглядывать в логи. Например, в `/var/log/apache2/shopware-error.log`

На этом обновление до 5й версии можно считать законченным.
