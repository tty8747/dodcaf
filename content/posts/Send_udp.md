---
author: "Sergey Chulanov"
title: "Send and recieve udp"
date: 2024-02-13T15:34:02+03:00
description: >-
  How to send udp-datagram and recieve it on windows and linux.
tags:
  - udp
categories:
  - linux
  - windows
  - devops
  - note
series:
  - guide
draft: false
---

Let's start listening udp on port 2193 and host 172.16.4.105:
```bash
sudo tcpdump -i ens18 udp port 2193 -vv -X
```

Send messages to listener:
```bash
echo -n "HELLO FROM LINUX|c" | nc -w 1 -u -4 172.16.4.105 2193
```

The same step, but for windows (powershell):
> [Install nmap](https://nmap.org/download) (includes netcat) before.
```bash
echo -n "HELLO FROM WINDOWS|c" | ncat -w 1 -u -4 172.16.4.105 2193
```

And we can see udp-datagrams on the listener:
```bash
sudo tcpdump -i ens18 udp port 2193 -vv -X
	tcpdump: listening on ens18, link-type EN10MB (Ethernet), capture size 262144 bytes
	12:29:04.426230 IP (tos 0x0, ttl 63, id 53708, offset 0, flags [DF], proto UDP (17), length 46)
	    10.10.100.51.58542 > nginx.dmz.lan.2193: [udp sum ok] UDP, length 18
		0x0000:  4500 002e d1cc 4000 3f11 4b3c 0a0a 6433  E.....@.?.K<..d3
		0x0010:  ac10 0469 e4ae 0891 001a 3f1c 4845 4c4c  ...i......?.HELL
		0x0020:  4f20 4652 4f4d 204c 494e 5558 7c63       O.FROM.LINUX|c
	12:29:18.639641 IP (tos 0x0, ttl 128, id 2181, offset 0, flags [none], proto UDP (17), length 54)
	    serv1c7.dmz.lan.58297 > nginx.dmz.lan.2193: [udp sum ok] UDP, length 26
		0x0000:  4500 0036 0885 0000 8011 d141 ac10 0467  E..6.......A...g
		0x0010:  ac10 0469 e3b9 0891 0022 6fef 2d6e 0d0a  ...i....."o.-n..
		0x0020:  4845 4c4c 4f20 4652 4f4d 2057 494e 444f  HELLO.FROM.WINDO
		0x0030:  5753 7c63 0d0a                           WS|c..
```

The best [link](https://www.markhneedham.com/blog/2012/07/15/tcpdump-learning-how-to-read-udp-packets/) about the cause.

