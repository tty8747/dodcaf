---
author: "Sergey Chulanov"
title: "OpenConnect"
date: 2024-01-02T01:47:03+03:00
description: >-
  OpenConnect
tags:
  - openconnect
  - vpn
categories:
  - linux
  - devops
series:
  - guide
draft: false
---

| Ubuntu 22.04

Install certbot
```bash
sudo snap install certbot --classic
```

> Example how to manual generate and [revoke certificate](https://letsencrypt.org/docs/revoking/)
> ```bash
> YOUR_DOMAIN=www.ubukubu.ru
> sudo certbot certonly --manual --preferred-challenges=dns -d ${YOUR_DOMAIN} -d nonexistent.${YOUR_DOMAIN}
> sudo certbot revoke --cert-path /etc/letsencrypt/live/${YOUR_DOMAIN}/fullchain.pem
> ```

Get the cert with key
```bash
YOUR_DOMAIN=www.ubukubu.ru
sudo certbot certonly --standalone --preferred-challenges http -d ${YOUR_DOMAIN}
```

Run docker container:
```bash
docker pull quay.io/aminvakil/ocserv
docker run --name ocserv --sysctl net.ipv4.ip_forward=1 --cap-add NET_ADMIN --security-opt no-new-privileges -p 443:443 -p 443:443/udp -v /etc/letsencrypt/live/${YOUR_DOMAIN}/privkey.pem:/etc/ocserv/certs/server-key.pem -v /etc/letsencrypt/live/${YOUR_DOMAIN}/fullchain.pem:/etc/ocserv/certs/server-cert.pem -d quay.io/aminvakil/ocserv
```

Enable camouflage and set secret:
```bash
SECRET="$(pwgen 16 1)"
docker exec ocserv sed -i '/^camouflage = /{s/false/true/}' /etc/ocserv/ocserv.conf
docker exec ocserv sed -i "/^camouflage_secret = /{s/mysecretkey/${SECRET}/}" /etc/ocserv/ocserv.conf
```

Add user:
```bash
docker exec -ti ocserv ocpasswd -c /etc/ocserv/ocpasswd goto
```

Delete default user `test`:
```bash
docker exec -ti ocserv ocpasswd -c /etc/ocserv/ocpasswd -d test
```

Restart the container:
```bash
docker restart ocserv
```
Connect to `$YOUR_DOMAIN/?$SECRET` (`echo $YOUR_DOMAIN/?$SECRET`) via openconnect client and enjoy

Also add in crontab this line:
```bash
0 4 * * 1 bash -c "docker restart ocserv"
```

## Clients
Cisco AnyConnect Client:
  - [Android](https://play.google.com/store/apps/details?id=com.cisco.anyconnect.vpn.android.avf&hl=en)
  - [Iphone](https://apps.apple.com/us/app/cisco-secure-client/id1135064690)
  - [Windows](https://its.gmu.edu/knowledge-base/how-to-install-cisco-anyconnect-on-a-windows-computer/)
  - [Windows](https://vc.vscht.cz/navody/sit/vpn/windows)

OneConnect:
  - [Developer's site](https://www.clavister.com/products/oneconnect/)
  - [Android](https://play.google.com/store/apps/details?id=com.clavister.oneconnect&hl=en_US)
  - [Apple](https://apps.apple.com/us/app/clavister-oneconnect/id1565970099)
  - [MS Store](https://apps.microsoft.com/detail/clavister-oneconnect/9P2L1BWS7BB6?hl=en-US&gl=US)

OpenConnect:
  - https://www.infradead.org/openconnect/vpnc-script.html
  - https://github.com/dlenski/vpn-slice
  - https://gitlab.com/openconnect/openconnect-gui/-/releases
  - https://drive.google.com/drive/folders/1KdzHlYODE-QSYL-JSQoM5vubEw55hhRi
  - [Android](https://play.google.com/store/apps/details?id=com.github.digitalsoftwaresolutions.openconnect&hl=en_US)
  - [Android](https://f-droid.org/ru/packages/app.openconnect/)

Using with http injector (Android):
- [Install anyconnect](https://play.google.com/store/apps/details?id=com.cisco.anyconnect.vpn.android.avf&hl=en&pli=1)
- [Install HTTP-injector]( https://play.google.com/store/apps/details?id=com.evozi.injector&hl=en_US)
- HTTP Injector: `Instruments` -> `Modem` -> `Hotshare` -> `step 1` -> `start Wi-Fi Hitspot` -> `run your vpn (on the phone)` -> `(back to HTTP injector) Start HOTSHARE`

Useful link:
- https://habr.com/ru/articles/776256/
