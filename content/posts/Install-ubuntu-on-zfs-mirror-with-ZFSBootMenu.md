---
author: "Sergey Chulanov"
title: "Install ubuntu on zfs mirror with ZFSBootMenu"
date: 2025-02-03T00:25:01+03:00
description: >-
  Install ubuntu on zfs mirror with ZFSBootMenu
tags:
  - dos
  - grep
  - ip
  - script
  - zfs
  - mirror
  - efi
categories:
  - linux
series:
  - guide
draft: false
---

| Ubuntu 24.04

- Main documentation [link](https://docs.zfsbootmenu.org/en/v2.3.x/guides/ubuntu/noble-uefi.html)

Run Ubuntu Server installation, choose enter shell, create user and start ssh daemon. Connect trough ssh and run scripts
```bash
useradd -s /bin/bash -m -G sudo goto
passwd goto
systemctl start ssh
ip -c a
```

Run the script after `sudo -Es`:
> Uses 2 disks `vda` and `vdb`, change them!
```bash
#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail
# set -o xtrace

# Link: https://docs.zfsbootmenu.org/en/v2.3.x/guides/ubuntu/noble-uefi.html

### Configure Live Environment
dmesg | grep -i efivars
source /etc/os-release
export ID

apt update
apt install --yes debootstrap gdisk zfsutils-linux

# Generate /etc/hostid
# zgenhostid -f 0x00bab10c
zgenhostid -f $(printf "0x%08x\n" $((RANDOM)))

### Define disk variables
export BOOT_DISK1="/dev/vda"
export BOOT_PART1="1"
export BOOT_DEVICE1="${BOOT_DISK1}${BOOT_PART1}"

export BOOT_DISK2="/dev/vdb"
export BOOT_PART2="1"
export BOOT_DEVICE2="${BOOT_DISK2}${BOOT_PART2}"

export POOL_DISK1="/dev/vda"
export POOL_PART1="2"
export POOL_DEVICE1="${POOL_DISK1}${POOL_PART1}"

export POOL_DISK2="/dev/vdb"
export POOL_PART2="2"
export POOL_DEVICE2="${POOL_DISK2}${POOL_PART2}"

echo "$BOOT_DEVICE1"
echo "$BOOT_DEVICE2"
echo "$POOL_DEVICE1"
echo "$POOL_DEVICE2"

### Disk preparation
# Skip it for clean disks
# zpool labelclear -f "$BOOT_DISK1"
# zpool labelclear -f "$BOOT_DISK2"
# zpool labelclear -f "$POOL_DISK1"
# zpool labelclear -f "$POOL_DISK2"

wipefs -a "$BOOT_DISK1"
wipefs -a "$BOOT_DISK2"
wipefs -a "$POOL_DISK1"
wipefs -a "$POOL_DISK2"

sgdisk --zap-all "$BOOT_DISK1"
sgdisk --zap-all "$BOOT_DISK2"
sgdisk --zap-all "$POOL_DISK1"
sgdisk --zap-all "$POOL_DISK2"

# Create EFI boot partitions
sgdisk -n "${BOOT_PART1}:1m:+1024m" -t "${BOOT_PART1}:ef00" "$BOOT_DISK1"
sgdisk -n "${BOOT_PART2}:1m:+1024m" -t "${BOOT_PART2}:ef00" "$BOOT_DISK2"

# Create zpool partition
sgdisk -n "${POOL_PART1}:0:-10m" -t "${POOL_PART1}:bf00" "$POOL_DISK1"
sgdisk -n "${POOL_PART2}:0:-10m" -t "${POOL_PART2}:bf00" "$POOL_DISK2"

parted "$POOL_DISK1" --script print
parted "$POOL_DISK2" --script print

### ZFS pool creation
# Create the zpool
zpool create -f -o ashift=12 \
  -O compression=lz4 \
  -O acltype=posixacl \
  -O xattr=sa \
  -O relatime=on \
  -o autotrim=on \
  -o compatibility=openzfs-2.1-linux \
  -m none zroot mirror "${POOL_DEVICE1}" "${POOL_DEVICE2}"

# Create initial file systems
zfs create -o mountpoint=none zroot/ROOT
zfs create -o mountpoint=/ -o canmount=noauto zroot/ROOT/${ID}
zfs create -o mountpoint=/home zroot/home

zpool set bootfs=zroot/ROOT/${ID} zroot

zfs get canmount
zpool get bootfs

# Export, then re-import with a temporary mountpoint of /mnt
zpool export zroot
zpool import -N -R /mnt zroot
zfs mount zroot/ROOT/${ID}
zfs mount zroot/home

# Verify that everything is mounted correctly
mount | grep mnt

# Update device symlinks
udevadm trigger

### Install Ubuntu
debootstrap noble /mnt

# Copy files into the new install
cp /etc/hostid /mnt/etc
cp /etc/resolv.conf /mnt/etc

# Chroot into the new OS
mount --types proc proc /mnt/proc
mount --types sysfs sys /mnt/sys
mount --bind /dev /mnt/dev
mount --types devpts pts /mnt/dev/pts
chroot /mnt /bin/bash
```

Create another script in chroot, and run
> Uses 2 disks `vda` and `vdb`, change them!
```bash
#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail
# set -o xtrace

### Define disk variables
export BOOT_DISK1="/dev/vda"
export BOOT_PART1="1"
export BOOT_DEVICE1="${BOOT_DISK1}${BOOT_PART1}"

export BOOT_DISK2="/dev/vdb"
export BOOT_PART2="1"
export BOOT_DEVICE2="${BOOT_DISK2}${BOOT_PART2}"

export POOL_DISK1="/dev/vda"
export POOL_PART1="2"
export POOL_DEVICE1="${POOL_DISK1}${POOL_PART1}"

export POOL_DISK2="/dev/vdb"
export POOL_PART2="2"
export POOL_DEVICE2="${POOL_DISK2}${POOL_PART2}"

echo "$BOOT_DEVICE1"
echo "$BOOT_DEVICE2"
echo "$POOL_DEVICE1"
echo "$POOL_DEVICE2"

### Basic Ubuntu Configuration
# Set a hostname
echo 'spot' > /etc/hostname
echo -e '127.0.1.1\tspot' >> /etc/hosts

# Set a root password
passwd

# Configure apt. Use other mirrors if you prefer.
cat <<EOF > /etc/apt/sources.list
# Uncomment the deb-src entries if you need source packages

deb http://archive.ubuntu.com/ubuntu/ noble main restricted universe multiverse
# deb-src http://archive.ubuntu.com/ubuntu/ noble main restricted universe multiverse

deb http://archive.ubuntu.com/ubuntu/ noble-updates main restricted universe multiverse
# deb-src http://archive.ubuntu.com/ubuntu/ noble-updates main restricted universe multiverse

deb http://archive.ubuntu.com/ubuntu/ noble-security main restricted universe multiverse
# deb-src http://archive.ubuntu.com/ubuntu/ noble-security main restricted universe multiverse

deb http://archive.ubuntu.com/ubuntu/ noble-backports main restricted universe multiverse
# deb-src http://archive.ubuntu.com/ubuntu/ noble-backports main restricted universe multiverse
EOF

# Update the repository cache and system
apt update --yes
apt upgrade --yes

# Install additional base packages
apt install --yes --no-install-recommends linux-generic locales keyboard-configuration console-setup

# Configure packages to customize local and console properties
dpkg-reconfigure locales tzdata keyboard-configuration console-setup

### ZFS Configuration
# Install required packages
apt install --yes dosfstools zfs-initramfs zfsutils-linux

# Enable systemd ZFS services
systemctl enable zfs.target
systemctl enable zfs-import-cache
systemctl enable zfs-mount
systemctl enable zfs-import.target

# Rebuild the initramfs
update-initramfs -c -k all

### Install and configure ZFSBootMenu
# Set ZFSBootMenu properties on datasets
zfs set org.zfsbootmenu:commandline="quiet" zroot/ROOT

# Create a vfat filesystem
mkfs.vfat -F32 "$BOOT_DEVICE1"
mkfs.vfat -F32 "$BOOT_DEVICE2"

# Copy ID to another dos partition
apt install dosfstools
DOS_ID=$(blkid "$BOOT_DEVICE1" | grep -oP '(?<=(UUID=")).*(?=" BLOCK)' | tr -d '-')
sudo mkdosfs -i "$DOS_ID" "$BOOT_DEVICE2"

# Create an fstab entry and mount
cat << EOF >> /etc/fstab
$( blkid | grep "$BOOT_DEVICE1" | cut -d ' ' -f 2 ) /boot/efi vfat defaults 0 0
EOF

mkdir -p /boot/efi
mount "${BOOT_DISK1}${BOOT_PART1}" /boot/efi

### Install ZFSBootMenu
apt install curl
mkdir -p /boot/efi/EFI/ZBM
curl -o /boot/efi/EFI/ZBM/VMLINUZ.EFI -L https://get.zfsbootmenu.org/efi
cp /boot/efi/EFI/ZBM/VMLINUZ.EFI /boot/efi/EFI/ZBM/VMLINUZ-BACKUP.EFI

umount /boot/efi
mount "${BOOT_DISK2}${BOOT_PART2}" /boot/efi

mkdir -p /boot/efi/EFI/ZBM
curl -o /boot/efi/EFI/ZBM/VMLINUZ.EFI -L https://get.zfsbootmenu.org/efi
cp /boot/efi/EFI/ZBM/VMLINUZ.EFI /boot/efi/EFI/ZBM/VMLINUZ-BACKUP.EFI

# Copy to another disk
# dd if="${BOOT_DISK1}${BOOT_PART1}" of="${BOOT_DISK2}${BOOT_PART2}" bs=64M oflag=sync status=progress

# Configure EFI boot entries
mount -t efivarfs efivarfs /sys/firmware/efi/efivars
apt install --yes efibootmgr

efibootmgr --create --disk "$BOOT_DISK1" --part 1 \
  --label "ZFSBootMenu" -l "\EFI\ZBM\VMLINUZ.EFI"

efibootmgr --create --disk "$BOOT_DISK1" --part 1 \
  --label "ZFSBootMenu (Backup)" -l "\EFI\ZBM\VMLINUZ-BACKUP.EFI"

efibootmgr --create --disk "$BOOT_DISK2" --part 1 \
  --label "ZFSBootMenu 2" -l "\EFI\ZBM\VMLINUZ.EFI"

efibootmgr --create --disk "$BOOT_DISK2" --part 1 \
  --label "ZFSBootMenu (Backup) 2" -l "\EFI\ZBM\VMLINUZ-BACKUP.EFI"
```


Prepare for first boot
```bash
exit
umount --no-mtab --recursive /mnt
```

Export the zpool and reboot
```bash
zpool export zroot
reboot
```

After reboot set up temporary network settings:
```bash
ip addr add 192.168.137.228/24 dev enp1s0

ip link set dev enp1s0 up
# ip link set dev enp1s0 down

ip route add default via 192.168.137.1


cat << EOF | sudo tee /etc/resolv.conf
nameserver 8.8.8.8
nameserver 8.8.4.4
EOF
```

Connect trought `ssh` and configure netplan

Boot EFI image in manual mode
```bash
# https://www.bootdev.ru/2018/10/EFI-Shell-What-to-do-if-the-OS-does-not-loaded.html

# list boot devices
map

# Choose device
FS0:

# examine dirs
ls EFI\ZBM\VMLINUZ.EFI

# starting boot
EFI\ZBM\VMLINUZ.EFI
FS0:EFI\ZBM\VMLINUZ.EFI
```

