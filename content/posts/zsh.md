---
author: "Sergey Chulanov"
title: "Установка и настройка оболочки zsh."
date: 2021-08-02T09:48:23+03:00
description: >-
  Как установить zsh и подключить плагин к этой оболочке.
tags:
  - zsh
categories:
  - linux
series:
  - guide
draft: false
---

## Установка `zsh`.

Обновим пакеты и установим необходимое:

```bash
$ sudo apt update
$ sudo apt install zsh
$ sudo apt install powerline fonts-powerline
```

> `Powerline` - это плагин строки состояния для `vim`, который предоставляет строки состояния и подсказки для нескольких других приложений, включая `zsh`, `bash`, `tmux`, `IPython`, `Awesome` и `Qtile`.

## Установка `Oh my zsh`.

Клонируем репозиторий `Oh my zsh` и создаём новый конфиг-файл `.zshrc`:

```bash
$ git clone https://github.com/robbyrussell/oh-my-zsh.git ~/.oh-my-zsh
$ cp ~/.oh-my-zsh/templates/zshrc.zsh-template ~/.zshrc
```

В `~/.zshrc` можно установить тему, добавить перемменные окружения или ещё что-то. Выбрать тему можно [здесь](https://github.com/ohmyzsh/ohmyzsh/wiki/Themes), я использую эти 2 темы:

```bash
ZSH_THEME="steeef"
# либо
ZSH_THEME="ys"
```

## Установка подсветки синтаксиса `Oh my zsh`.

Клонируем репозиторий и добавляем `syntax-highlighting` в файл конфигурации `~/.zshrc`:

```bash
$ echo "source $HOME/.zsh-syntax-highlighting/zsh-syntax-highlighting.zsh" >> "$HOME/.zshrc"
$ echo "source $HOME/.zsh-syntax-highlighting/zsh-syntax-highlighting.zsh" >> "$HOME/.zshrc"
```

## Установка плагина автодополнения для `zsh`.

Клонируем репозиторий `zsh-autosuggestions`.

```bash
$ git clone https://github.com/zsh-users/zsh-autosuggestions ~/.oh-my-zsh/custom/plugins/zsh-autosuggestions
```

В файле `~/.zshrc` меняем строку `plugins=(git)` на `plugins=(git zsh-autosuggestions)`. После сохранения нужно перелогиниться или перечитать конфиг `~/.zshrc`.

## Установка командной оболочки по-умолчанию.

Сделать это можно с помощью утилиты `chsh`:

```bash
# сменить на zsh
$ chsh -s /bin/zsh
# сменить на bash
$ chsh -s /bin/bash
```
## Установка `fzf.zsh` для более удобного поиска по `Ctrl + r`.

Клонируем репозиторий, устанавливаем и перечитываем файл конфигурации `~/.zshrc`:

```bash
$ git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
$ ~/.fzf/install
$ source ~/.zshrc
```
Мои текущие настройки в файле `~/.zshrc`:

```bash
# myuser - Заменить на вашего пользователя

export ZSH="/home/myuser/.oh-my-zsh"
ZSH_THEME="ys"
plugins=(git zsh-autosuggestions zsh-syntax-highlighting)
source $ZSH/oh-my-zsh.sh
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
export PATH=$PATH:$HOME/.local/bin
eval $(~/.linuxbrew/bin/brew shellenv)
export GOROOT=/usr/local/go
export GOPATH=$HOME/go
export PATH=$GOPATH/bin:$GOROOT/bin:$PATH
autoload -U +X bashcompinit && bashcompinit
complete -o nospace -C /usr/bin/terraform terraform
export TF_VAR_do_token="There is Digital Ocean token for Terraform"
export TF_VAR_HC_TOKEN="There is Hetzner token for Terraform"
export HC_KEY="There is Hetzner token"
export VSCALE_KEY="There is Vscale token"
if [ -f '/home/myuser/yandex-cloud/path.bash.inc' ]; then source '/home/myuser/yandex-cloud/path.bash.inc'; fi
if [ -f '/home/myuser/yandex-cloud/completion.zsh.inc' ]; then source '/home/myuser/yandex-cloud/completion.zsh.inc'; fi
# настройки автодополнения в текущую сессию zsh
source <(kubectl completion zsh)
source <(doctl completion zsh)
```
