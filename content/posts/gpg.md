---
author: "Sergey Chulanov"
title: "gpg"
date: 2024-01-02T01:31:00+03:00
description: >-
  How to add gpg-keys in Ubuntu and make bin from txt.
tags:
  - gpg
  - apt
categories:
  - linux
  - devops
series:
  - guide
draft: false
---

| Ubuntu 22.04

- we need to use `/usr/share/keyrings` to keep keys
- for key managed local - `/etc/apt/keyrings` (need to create it by own with 755 permissions)
- key should be in binary shape and have the name: `repo-archive-keyring.gpg`
- also we should have `deb [signed-by=/usr/share/keyrings/repo-archive-keyring.gpg] ...` in the line of repo

> Example for `mongo` with `amd64,arm64`:
> ```bash
> deb [ arch=amd64,arm64 signed-by=/usr/share/keyrings/mongodb-server-7.0.gpg ] https://repo.mongodb.org/apt/ubuntu jammy/mongodb-org/7.0 multiverse
> ```

## Determinating the type of key:
```bash
file repo_signing.key
# - text format:	PGP public key block Public-Key (old)
# - binary format:	OpenPGP Public Key Version 4
```

Also we need to delete old key before install new:
```bash
apt-key list
sudo apt-key del "KEY-ID"
sudo apt-key del "18DF3741CDFFDE29"
	Warning: apt-key is deprecated. Manage keyring files in trusted.gpg.d instead (see apt-key(8)).
	OK
```

## Anydesk ([link](http://deb.anydesk.com/howto.html)):
```bash
curl https://keys.anydesk.com/repos/DEB-GPG-KEY -o anydesk-archive-keyring.key
cat anydesk-archive-keyring.key

file anydesk-archive-keyring.key
	anydesk-archive-keyring.key: PGP public key block Public-Key (old)

# make binanry format
gpg --dearmor < anydesk-archive-keyring.key | sudo tee /usr/share/keyrings/anydesk-archive-keyring.gpg 1>/dev/null
file /usr/share/keyrings/anydesk-archive-keyring.gpg                           
	/usr/share/keyrings/anydesk-archive-keyring.gpg: OpenPGP Public Key Version 4, Created Tue Dec 19 08:19:58 2017, RSA (Encrypt or Sign, 2048 bits); User ID; Signature; OpenPGP Certificate

# more shortly
sudo mkdir -pv /etc/apt/keyrings
sudo chmod 755 /etc/apt/keyrings
curl https://keys.anydesk.com/repos/DEB-GPG-KEY | gpg --dearmor | sudo tee /etc/apt/keyrings/anydesk-archive-keyring.gpg 1>/dev/null
file /etc/apt/keyrings/anydesk-archive-keyring.gpg
```

And let's add **Anydesk** repository:
```bash
echo "deb [arch=amd64 signed-by=/etc/apt/keyrings/anydesk-archive-keyring.gpg] http://deb.anydesk.com/ all main" | sudo tee /etc/apt/sources.list.d/anydesk-stable.list
sudo apt update -y
```

> If you already have binary key - you need only put the key into `/etc/apt/keyrings` or `/usr/share/keyrings` directories

## Get key form the server of keys (rarely scenario):
> You should know key id and key server
```bash
# find
gpg --keyserver keyserver.ubuntu.com --recv "KEY-ID"

# get binary key
gpg --export "KEY-ID" | sudo tee /usr/share/keyrings/repo-archive-keyring.gpg >/dev/null

# get txt key
gpg --export --armor "KEY-ID" | sudo tee /usr/share/keyrings/repo-archive-keyring.asc >/dev/null

# get and export
sudo gpg --no-default-keyring --keyring /usr/share/keyrings/repo-archive-keyring.gpg --keyserver keyserver.ubuntu.com --recv "KEY-ID"
```

Deleting keys:
```bash
# It's enough delete file from directory:
sudo rm -f /usr/share/keyrings/repo-archive-keyring.gpg
```

Short example for terraform:
```bash
curl https://apt.releases.hashicorp.com/gpg -o hashicorp-archive-keyring.gpg
file hashicorp-archive-keyring.gpg
gpg --dearmor < hashicorp-archive-keyring.gpg | sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg 1>/dev/null
```
