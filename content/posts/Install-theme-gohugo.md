---
author: "Sergey Chulanov"
title: "Install theme gohugo"
date: 2023-08-26T21:43:20+03:00
description: >-
  How to install new theme
tags:
  - hugo
categories:
  - linux
series:
  - guide
draft: false
---

# Install theme gohugo:

- [Official instructions](https://gohugobrasil.netlify.app/themes/installing-and-using-themes/)

```bash
# cd themes; git clone git@github.com:dillonzq/LoveIt.git; cd -
git submodule add git@github.com:dillonzq/LoveIt.git themes/LoveIt
```

Try new theme:
```bash
hugo server -t LoveIt
```

Set theme `LoveIt`:
```bash
echo "theme = 'LoveIt'" >> hugo.toml
hugo server
```
