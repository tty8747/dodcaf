---
author: "Sergey Chulanov"
title: "Ansible. Заметка об использовании gather_facts."
date: 2021-07-31T10:11:16+03:00
description: >-
  Короткая заметка как вывести все gather_facts в ansible
tags:
  - ansible
categories:
  - devops
  - ops
series:
  - note
draft: false
---

В `ansible` при запуске плейбука изначально выполняется модуль `ansible.builtin.setup`, который собирает факты о системе, где будет выполняться данный плейбук, если явно не указано `gather_facts: no`.
Все факты хранятся внутри переменной `ansible_facts`, выведем её:

```bash
- name: Print all available facts
  debug:
    var: ansible_facts
```

Обычно это временная мера, чтобы подобрать переменные, для использования внутри `playbook`.  Основываясь на этом, можно получить `версию ОС` и `fqdn`:

```bash
- name: Output OS
  debug:
    msg: "{{ ansible_facts['distribution'] }}-{{ ansible_facts['distribution_major_version'] }}"

- name: Output fqdn
  debug:
    msg: "{{  ansible_facts.fqdn }}"
```

Хотя для просмотра фактов, возможно, более подходящим решением будет не загонять это внутрь `playbook`, а использовать `ansible`:

```bash
$ ansible -i inventory/hosts.yml all -m gather_facts --tree /tmp/facts
```

Это сохранит факты о хостах, описанных в `inventory/hosts.yml` в папке `/tmp/facts`.  
Для удобства просмотра, можно использовать `jq`:

```bash
$ cat /tmp/facts/nginx01-tty8747 | jq . | head -20
{
  "ansible_facts": {
    "ansible_all_ipv4_addresses": [
      "10.135.0.14",
      "64.225.105.161",
      "10.19.0.17"
    ],
    "ansible_all_ipv6_addresses": [
      "fe80::60a2:b9ff:feb9:e68e",
      "fe80::e4e4:bfff:fe6e:945"
    ],
    "ansible_apparmor": {
      "status": "enabled"
    },
    "ansible_architecture": "x86_64",
    "ansible_bios_date": "12/12/2017",
    "ansible_bios_version": "20171212",
    "ansible_cmdline": {
      "BOOT_IMAGE": "/boot/vmlinuz-5.4.0-73-generic",
      "console": "ttyS0",
```

Как пример, хочу привести использование модуля `raw`:

```bash
$ ansible -i inventory/hosts.yml all -m raw -a "cat /etc/*release" | head
nodename | CHANGED | rc=0 >>
DISTRIB_ID=Ubuntu
DISTRIB_RELEASE=20.04
DISTRIB_CODENAME=focal
DISTRIB_DESCRIPTION="Ubuntu 20.04.2 LTS"
NAME="Ubuntu"
VERSION="20.04.2 LTS (Focal Fossa)"
ID=ubuntu
ID_LIKE=debian
PRETTY_NAME="Ubuntu 20.04.2 LTS"
```
