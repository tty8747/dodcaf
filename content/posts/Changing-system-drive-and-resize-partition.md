---
author: "Sergey Chulanov"
title: "Changing system drive and resize partition"
date: 2023-02-20T21:55:27+03:00
description: >-
  Changing system drive and resize partition on proxmox virtualization.
tags:
  - lvm
  - fdisk
  - dd
categories:
  - linux
  - ops
draft: false
---
# Changing system drive and resize partition

## Run `lsblk` to see our drives:
```bash
NAME                MAJ:MIN RM   SIZE RO TYPE  MOUNTPOINT
loop0                 7:0    0    28G  0 loop  
loop1                 7:1    0   800G  0 loop  
loop2                 7:2    0   170G  0 loop  
sda                   8:0    0   1,8T  0 disk
sdb                   8:16   0 447,1G  0 disk  
└─sdb1                8:17   0 447,1G  0 part  
  └─md0               9:0    0   447G  0 raid1 /mnt/pve/ssdraid
sdc                   8:32   0 931,5G  0 disk  
└─sdc1                8:33   0 931,5G  0 part  /mnt/pve/single_backup
sdd                   8:48   0 447,1G  0 disk  
└─sdd1                8:49   0 447,1G  0 part  
  └─md0               9:0    0   447G  0 raid1 /mnt/pve/ssdraid
sde                   8:64   0 931,5G  0 disk  
├─sde1                8:65   0   243M  0 part  /boot
├─sde2                8:66   0     1K  0 part  
└─sde5                8:69   0 931,3G  0 part  
  ├─iron--vg-root   253:0    0 907,3G  0 lvm   /
  └─iron--vg-swap_1 253:1    0    24G  0 lvm   [SWAP]
zram0               252:0    0     2G  0 disk  [SWAP]
```

- `/dev/sde` is our system drive
- `/dev/sda` is our new drive (I bought 2TB drive)

## Let's copy with `dd` all of data from `/dev/sde` to `/dev/sda`. Run it inside virtual terminal `tmux`:
> If there are running virtual machines - stop it, otherwise we can get **read only file system**.
```bash
tmux
dd if=/dev/sde of=/dev/sda bs=64K conv=noerror,sync oflag=sync status=progress
```
<!--
## ~~Get the table structure~~
```bash
sfdisk –dump /dev/sde > sde-tables.txt
```
> We can apply it on new disk `/dev/sda`
> ```bash
> sudo sfdisk /dev/sda < sde-tables.txt
> ```
-->
We can check the filesystem:
```bash
sudo vi /etc/default/grub
GRUB_CMDLINE_LINUX_DEFAULT="quiet splash fsck.mode=force fsck.repair=yes"
sudo update-grub
sudo reboot
```

> After reboot our new disk is `/dev/sde`

Resize the volume from 1Tb to 2Tb. Go to `fdisk`:
`sudo fdisk /dev/sde`
```bash
Command (m for help): p
Disk /dev/sde: 1,8 TiB, 2000398934016 bytes, 3907029168 sectors
Disk model: WDC WD2003FZEX-0
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 4096 bytes
I/O size (minimum/optimal): 4096 bytes / 4096 bytes
Disklabel type: dos
Disk identifier: 0x586fbcce

Device     Boot  Start        End    Sectors  Size Id Type
/dev/sde1  *      2048     499711     497664  243M 83 Linux
/dev/sde2       501758 3907029167 3906527410  1,8T  5 Extended
/dev/sde5       501760 3907029167 3906527408  1,8T 8e Linux LVM

Partition 2 does not start on physical sector boundary.

Command (m for help): d -> 5
Partition 5 has been deleted.

Command (m for help): d -> 2
Partition 2 has been deleted.

Command (m for help): n -> e
Partition type
   p   primary (1 primary, 0 extended, 3 free)
   e   extended (container for logical partitions)
Select (default p): e -> 2
Partition number (2-4, default 2): 
First sector (499712-3907029167, default 499712): 
Last sector, +/-sectors or +/-size{K,M,G,T,P} (499712-3907029167, default 3907029167): 

Created a new partition 2 of type 'Extended' and of size 1,8 TiB.

Command (m for help): n
All space for primary partitions is in use.
Adding logical partition 5
First sector (501760-3907029167, default 501760): 
Last sector, +/-sectors or +/-size{K,M,G,T,P} (501760-3907029167, default 3907029167): 

Created a new partition 5 of type 'Linux' and of size 1,8 TiB.
Partition #5 contains a LVM2_member signature.

Do you want to remove the signature? [Y]es/[N]o: N

Command (m for help): p

Disk /dev/sde: 1,8 TiB, 2000398934016 bytes, 3907029168 sectors
Disk model: WDC WD2003FZEX-0
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 4096 bytes
I/O size (minimum/optimal): 4096 bytes / 4096 bytes
Disklabel type: dos
Disk identifier: 0x586fbcce

Device     Boot  Start        End    Sectors  Size Id Type
/dev/sde1  *      2048     499711     497664  243M 83 Linux
/dev/sde2       499712 3907029167 3906529456  1,8T  5 Extended
/dev/sde5       501760 3907029167 3906527408  1,8T 83 Linux

Command (m for help): w
The partition table has been altered.
Calling ioctl() to re-read partition table.
Syncing disks.
```

So it was:
```bash
sde                   8:64   0   1,8T  0 disk  
├─sde1                8:65   0   243M  0 part  /boot
├─sde2                8:66   0     1K  0 part  
└─sde5                8:69   0 931,3G  0 part  
  ├─iron--vg-root   253:0    0 907,3G  0 lvm   /
  └─iron--vg-swap_1 253:1    0    24G  0 lvm   [SWAP]
```

And now it is:
```bash
sde                   8:64   0   1,8T  0 disk  
├─sde1                8:65   0   243M  0 part  /boot
└─sde5                8:69   0   1,8T  0 part  
  ├─iron--vg-root   253:0    0 907,3G  0 lvm   /
  └─iron--vg-swap_1 253:1    0    24G  0 lvm   [SWAP]
```


LVM:  
Resize pvs:
```bash
sudo pvs
  PV         VG      Fmt  Attr PSize    PFree
  /dev/sde5  iron-vg lvm2 a--  <931,27g    0 

sudo pvresize /dev/sde5
  Physical volume "/dev/sde5" changed
  1 physical volume(s) resized or updated / 0 physical volume(s) not resized

sudo pvs
  PV         VG      Fmt  Attr PSize  PFree  
  /dev/sde5  iron-vg lvm2 a--  <1,82t 931,50g
```

Resize lvs:
```bash
sudo lvs
  LV     VG      Attr       LSize    Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  root   iron-vg -wi-ao---- <907,33g
  swap_1 iron-vg -wi-ao----   23,94g

sudo lvextend -l +100%FREE /dev/iron-vg/root
  Size of logical volume iron-vg/root changed from <907,33 GiB (232276 extents) to <1,80 TiB (470741 extents).
  Logical volume iron-vg/root successfully resized.

sudo lvs
  LV     VG      Attr       LSize  Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  root   iron-vg -wi-ao---- <1,80t
  swap_1 iron-vg -wi-ao---- 23,94g
```

And resize filesystem:
```bash
df -BG /
Файловая система          1G-блоков Использовано Доступно Использовано% Cмонтировано в
/dev/mapper/iron--vg-root      893G         457G     391G           54% /

sudo resize2fs /dev/iron-vg/root
resize2fs 1.44.5 (15-Dec-2018)
Filesystem at /dev/iron-vg/root is mounted on /; on-line resizing required
old_desc_blocks = 114, new_desc_blocks = 230
The filesystem on /dev/iron-vg/root is now 482038784 (4k) blocks long.

df -BG /
Файловая система          1G-блоков Использовано Доступно Использовано% Cмонтировано в
/dev/mapper/iron--vg-root     1809G         457G    1270G           27% /
```
