---
author: "Sergey Chulanov"
title: "Установка 3proxy из исходников."
date: 2021-07-30T21:12:42+03:00
description: >-
  Сборка и установка программы из исходников
tags:
  - 3proxy
  - build
categories:
  - linux
series:
  - guide
draft: false
---

- [Официальная документация](https://3proxy.ru/documents/)
- [Пример конфигурации](https://3proxy.ru/doc/ru/example1.txt)

## Установим пакет для компиляции программ:

```bash
$ apt-get install build-essential
```

## Скачаем исходники [отсюда](https://github.com/z3APA3A/3proxy):

```bash
$ wget https://github.com/z3APA3A/3proxy/archive/refs/tags/0.9.3.tar.gz -O 3proxy093.tgz
$ tar -zxvf 3proxy093.tgz
$ cd 3proxy-0.9.3
```

## Собираем:

```bash
$ sudo apt install checkinstall
$ make -f Makefile.Linux
```

## Создадим учётную запись для `3proxy`:

```bash
$ sudo adduser --system --disabled-login --no-create-home --group proxy3
```

## Копируем файл в `/usr/bin/`, а так же создаём директорию для log-файлов:

```bash
$ sudo mkdir -pv /var/log/3proxy /etc/3proxy
$ sudo cp -v bin/3proxy /usr/bin/
```

## Установим права:

```bash
$ sudo chown -v proxy3:proxy3 -R /etc/3proxy /usr/bin/3proxy /var/log/3proxy
```

## В конфиге нужно указать `setuid` и `setgid`. Узнаем, какие у нас:

```bash
id proxy3
uid=126(proxy3) gid=136(proxy3) groups=136(proxy3)
```

## Создадим конфиг:

```bash
cat <<- EOF | tee /etc/3proxy/3proxy.cfg
setgid 136
setuid 126

nserver ns1.zavod.lan
# nserver 192.168.8.1
# nserver 8.8.8.8

nscache 65536
timeouts 1 5 30 60 180 1800 15 60

# ip внешнего интерфейса
external 91.122.226.82
# ip внутреннего интерфейса
internal 192.168.8.1

daemon

log /var/log/3proxy/3proxy.log D
logformat "- +_L%t.%. %N.%p %E %U %C:%c %R:%r %O %I %h %T"
rotate 30

auth none

allow * * * 80-88,8080-8088 HTTP
allow * * * 443,8443 HTTPS

proxy -n
EOF
```

## Запускаем `3proxy` и проверяем, что всё работает:

```bash
$ sudo /usr/bin/3proxy /etc/3proxy/3proxy.cfg
```

## Создадим `systemd` файл для запуска `unit`:

> Как создавать `unit` файлы, я почитал [тут](https://linux-notes.org/pishem-systemd-unit-fajl/).

```bash
cat <<- EOF | sudo tee /etc/systemd/system/3proxy.service
[Unit]
Description=3proxy Proxy Server
After=syslog.target network.target

[Service]
Type=simple
User=proxy3
Group=proxy3
ExecStart=/usr/bin/3proxy /etc/3proxy/3proxy.cfg
RemainAfterExit=yes
Restart=on-failure

[Install]
WantedBy=multi-user.target
EOF
```

Перечитаем конфиг файлы юнитов, установим автозапуск и запустим:

```bash
$ sudo systemctl daemon-reload
$ sudo systemctl enable 3proxy
$ sudo systemctl start 3proxy
$ journalctl --unit=3proxy
```

Настроим аутентификацию:

- `CL` - пароль в виде текста (plain text)
- `CR` - зашифрованный пароль (md5)
- `NT` - пароль в формате NT

> При использовании зашированного пароля с символами `$` нужно его заключить в кавычки.

Генерируем пароль `die3Aipa` с солью `Kkyp7`:

```bash
$ openssl passwd -1 -salt Kkyp7
Password:
$1$Kkyp7$qZgpu9h7vi9.raAZl..Fd/
```

Добавим в файл `/etc/3proxy/3proxy.cfg` следующие строки:

> `cat <<- EOF | sudo tee --append /etc/3proxy/3proxy.cfg` не корректно переносит данные со спец символами.

```bash
users test01:CL:test01
users test03:CL:test03
users "test04:CR:$1$Kkyp7$qZgpu9h7vi9.raAZl..Fd/"
users "test05:CR:$1$Kkyp7$Z42v1BMb8dbN5Ou/mgVJa."
```

> Обратите внимае у пользователей `test04` и `test05` использована одинаковая соль. Соль используется, чтобы получить разные результаты для одинаковых паролей. Для каждого пользователя нужно использовать разную соль.

Изменим параметр `auth` в `/etc/3proxy/3proxy.cfg` для активации.
Возможные варианты:
- `none` - без авторизации.
- `iponly` - авторизация по IP-адресу клиента.
- `nbname` - по Netbios имени.
- `strong` - по логину и паролю.

```bash
# auth none
auth strong
```

Добавим ораничение скорости:

```bash
$ cat <<- EOF | sudo tee --append /etc/3proxy/3proxy.cfg

# ограничение скорости в 1 Мбит/с (1024 * 1024)
bandlimin 1048576 odpantus
# ограничение скорости в 5 Мбит/с (5 * 1024 * 1024)
# bandlimin 5242880 user1,user2
EOF
```

Перезапустим `3proxy`

```bash
$ sudo systemctl restart 3proxy
```

## SOCKS5 proxy (не заработал)

```bash
$ cat <<- EOF | sudo tee --append /etc/3proxy/3proxy.cfg

socks -p1080 -i192.168.8.1 -e91.122.226.82
EOF
```

- `-p1080` - 1080 стандартные порт для SOCKS5
- `-i192.168.8.1` - ip-address внутреннего интерфейса
- `-e91.122.226.82` - ip-address внешнего интерфейса
