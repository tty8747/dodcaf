---
author: "Sergey Chulanov"
title: "Настройка мониторинга Prometheus + Grafana."
date: 2021-09-24T16:13:09+03:00
description: >-
  Настройка мониторинга Prometheus + Grafana.
tags:
  - monitoring
  - prometheus
  - grafana
categories:
  - devops
series:
  - guide
draft: false
---

Эта картинка показывает архитектуру `prometheus`. Подробнее в [официальной документации](https://prometheus.io/docs/introduction/overview/):

![architecture](/images/architecture.png "Архитектура prometheus")

## Установка сервера `prometheus`:

1 Создадим виртуалку на proxmox"

```bash
$ sudo qm create 500 -agent enabled=1 -arch x86_64 -bwlimit 80 -kvm 1 --memory 2048 -name node-test -onboot 1 -cores 2 -net0 virtio,bridge=vmbr0 -virtio0 zpool:32 -cdrom local:iso/ubuntu-20.04.2-live-server-amd64.iso -bootdisk virtio0 -scsihw virtio-scsi-pci -boot order=ide2\;virtio0 -ostype l26
```

> [More info](https://pve.proxmox.com/pve-docs/qm.1.html)
> qm showcmd 300 -pretty |grep mac=
> qm destroy 300 --purge

Забираем [отсюда](https://github.com/prometheus/prometheus/releases) нужный релиз:

```bash
$ wget https://github.com/prometheus/prometheus/releases/download/v2.30.0/prometheus-2.30.0.linux-amd64.tar.gz
```

Теперь нужно создадть необходимые папки и пользователя, а так же положить бинарные файлы в папку, откуда они будут исполнятся. Для собранных файлов используется каталог `/usr/local/bin`:

```bash
$ tar -zxf prometheus-2.30.0.linux-amd64.tar.gz
$ sudo mv prometheus-2.30.0.linux-amd64/{prometheus,promtool} -t /usr/local/bin
$ sudo mkdir /usr/local/etc/prometheus /var/lib/prometheus
$ sudo mv prometheus-2.30.0.linux-amd64/* -t /usr/local/etc/prometheus
$ sudo useradd -M -s /bin/false prometheus
$ sudo chown -Rv prometheus:prometheus /var/lib/prometheus
```

> Примеры консолей здесь: `console_libraries/` и `consoles`, подробнее - [в документации](https://prometheus.io/docs/visualization/consoles/).


Теперь нужно создать `systemd-unit`:

```bash
$ cat <<- EOF | sudo tee /etc/systemd/system/prometheus.service
[Unit]
Description=Prometheus
Wants=network-online.target
After=network-online.target

[Service]
User=prometheus
Group=prometheus
Restart=on-failure
ExecStart=/usr/local/bin/prometheus \\
  --config.file /usr/local/etc/prometheus/prometheus.yml \\
  --storage.tsdb.path /var/lib/prometheus/ \\
  --storage.tsdb.retention.size 26GB
ExecReload=/bin/kill -HUP \$MAINPID
ProtectHome=true
ProtectSystem=full

[Install]
WantedBy=multi-user.target
EOF
```

Запускаем `prometheus`:

```bash
$ sudo systemctl daemon-reload
$ sudo systemctl enable prometheus
$ sudo systemctl start prometheus
$ sudo systemctl status prometheus
```

Проверим, нужно подключиться с пробросом порта 9090 и зайти в браузере на `http://localhost:9090`

```bash
$ ssh prometheus -L 9090:localhost:9090
```

## Установка `node_exporter`:

`node_exporter` нужно запустить на всех машинах, ресурсы которых мы хотим мониторить. Устанавливается примерно так же как и сервер. Скачиваем необходимый релиз [отсюда](https://github.com/prometheus/node_exporter/releases)

```bash
$ wget https://github.com/prometheus/node_exporter/releases/download/v1.2.2/node_exporter-1.2.2.linux-amd64.tar.gz
$ tar -zxf node_exporter-1.2.2.linux-amd64.tar.gz node_exporter-1.2.2.linux-amd64/node_exporter
$ sudo mv node_exporter-1.2.2.linux-amd64/node_exporter /usr/local/bin/
$ sudo useradd -M -s /bin/false node_exporter
```

Создадим `systemd-unit`:

> Если `/home` вынесен на отдельный раздел, то нужно убрать `ProtectHome=yes`, иначе `node_exporter` будет неправильно показывать оставшееся место.

```bash
$ cat <<- EOF | sudo tee /etc/systemd/system/node_exporter.service
[Unit]
Description=Prometheus node_exporter
After=network.target

[Service]
Type=simple
User=node_exporter
Group=node_exporter
ExecStart=/usr/local/bin/node_exporter

SyslogIdentifier=node_exporter
Restart=always

PrivateTmp=yes
ProtectHome=yes
NoNewPrivileges=yes

ProtectSystem=strict
ProtectControlGroups=true
ProtectKernelModules=true
ProtectKernelTunables=yes

[Install]
WantedBy=multi-user.target
EOF
```

Запустим `node_exporter`:

```bash
$ sudo systemctl daemon-reload
$ sudo systemctl enable node_exporter
$ sudo systemctl start node_exporter
$ sudo systemctl status node_exporter
```

Проверим, что метрики отдаются:
```bash
$ curl -s http://localhost:9100/metricscurl -s http://localhost:9100/metrics
```

## Установка `alertmanager`:

В `alertmanager` `prometheus` будет отдавать алерты, которые в конечном итоге могут быть доставлены нам, по электронной почте, telegram или pagerduty.

Скачать релез можно [отсюда](https://github.com/prometheus/alertmanager/releases). Затем запускаем по старой схеме:

```bash
$ wget https://github.com/prometheus/alertmanager/releases/download/v0.23.0/alertmanager-0.23.0.linux-amd64.tar.gz
$ sudo mkdir /usr/local/etc/alertmanager /var/lib/alertmanager
$ tar -zxf alertmanager-0.23.0.linux-amd64.tar.gz
$ sudo mv alertmanager-0.23.0.linux-amd64/{alertmanager,amtool} /usr/local/bin/
$ sudo mv alertmanager-0.23.0.linux-amd64/* /usr/local/etc/alertmanager
$ sudo useradd -M -s /bin/false alertmanager
$ sudo chown -Rv alertmanager:alertmanager /var/lib/alertmanager
```

Создадим `systemd-unit`:

```bash
$ cat <<- EOF | sudo tee /etc/systemd/system/alertmanager.service
[Unit]
Description=Alertmanager for prometheus
After=network.target

[Service]
User=alertmanager
ExecStart=/usr/local/bin/alertmanager \\
  --config.file=/usr/local/etc/alertmanager/alertmanager.yml \\
  --storage.path=/var/lib/alertmanager/
ExecReload=/bin/kill -HUP \$MAINPID

NoNewPrivileges=true
ProtectHome=true
ProtectSystem=full

[Install]
WantedBy=multi-user.target
EOF
```

Запустим `alermanager`:

```bash
$ sudo systemctl daemon-reload
$ sudo systemctl enable alertmanager
$ sudo systemctl start alertmanager
$ sudo systemctl status alertmanager
```

Веб-интерфейс `alertmanager`а доступен на порту 9093:

```bash
$ curl "http://localhost:9093"
```

## Установка `grafana`:

[Скачиваем](https://grafana.com/grafana/download) и устанавливаем `grafana` из пакета:

```bash
$ sudo apt-get install -y adduser libfontconfig1
$ wget https://dl.grafana.com/enterprise/release/grafana-enterprise_8.1.5_amd64.deb
$ sudo dpkg --install grafana-enterprise_8.1.5_amd64.deb
$ sudo /bin/systemctl daemon-reload
$ sudo /bin/systemctl enable grafana-server
$ sudo systemctl start grafana-server
$ sudo systemctl status grafana-server
```

А `grafana` слушает на порту 3000:

```bash
$ curl "http://localhost:3000/login"
```

> **Внимание!** `Prometheus`, `node_exporter`, `alertmanager` и `grafana` по-умолчанию висят на всех сетевых интерфейсах. Поэтому желательно прикрыться файерволлом или сделать проброс с http-based аутентификацией на `nginx`.

## Теперь нужно настроить связь между компонентами:

Приведём файл конфигурации к следующему виду:

```bash
$ cat /usr/local/etc/prometheus.yml
global:
  scrape_interval:     15s
  evaluation_interval: 15s

# Alertmanager configuration
alerting:
  alertmanagers:
  - static_configs:
    - targets:
       - localhost:9093

scrape_configs:
  - job_name: 'node'
    static_configs:
    - targets:
       - 192.168.1.194:9100
     # - anotherhost:9100

```

Небольшое описание:
- `scrape_interval` - периодичность, с которой `prometheus` будет забирать метрики.
- `localhost:9093` и `192.168.1.194:9100` - адреса `alertmanager` и `node_exporter` соответственно. Если адресов `node_exporter` больше, то нужно указать их все, как в примере выше.

После изменения конфига попросим `prometheus` перечитать его:

```bash
$ systemctl reload prometheus
```

Теперь в [веб-интерфейсе](http://localhost:9090/targets) `prometheus` должен появиться первый таргет. Но метрики обычно смотрят в веб-интерфейсе `grafana`. Стандартный логин и пароль: `admin:admin`. Здесь нужно в качестве `Data source` указать адрес нашего `prometheus`, а затем можно добавить готовый дашбоард [Node Exporter Full](https://grafana.com/grafana/dashboards/1860).

> Огромное множество дашбордов можно найти по [этой ссылке](https://grafana.com/grafana/dashboards)


### Полезное:

> [Ссылка на блог](https://www.robustperception.io/monitoring-is-a-means-not-an-end#more-5873) одного из разработчиков `prometheus`.
