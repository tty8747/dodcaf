---
author: "Sergey Chulanov"
title: "Minio: local start"
date: 2023-03-19T17:28:04+03:00
description: >-
  Bumping into minio
tags:
  - minio
  - minica
  - docker
  - java
categories:
  - linux
  - java
  - devops
  - guide
series:
  - guide
draft: false
---

### Quick links

| [Docker](#run-docker-with-minio) | [Linux](#run-on-linux-with-minica) | [Minica](#run-on-linux-with-minica) | [API](#work-with-api)

## Run for Windows:

Minio wrote on golang, so we need only get [binary file](https://dl.min.io/server/minio/release/windows-amd64/minio.exe) and run it, according [official docs](https://min.io/docs/minio/windows/index.html):
```shell
.\minio.exe server C:\minio --console-address :9001
	Formatting 1st pool, 1 set(s), 1 drives per set.
	WARNING: Host local has more than 0 drives of set. A host failure will result in data becoming unavailable.
	WARNING: Detected default credentials 'minioadmin:minioadmin', we recommend that you change these values with 'MINIO_ROOT_USER' and 'MINIO_ROOT_PASSWORD' environment variables
	MinIO Object Storage Server
	Copyright: 2015-2023 MinIO, Inc.
	License: GNU AGPLv3 <https://www.gnu.org/licenses/agpl-3.0.html>
	Version: RELEASE.2023-03-13T19-46-17Z (go1.19.7 windows/amd64)

	Status:         1 Online, 0 Offline.
	API: http://192.168.137.217:9000  http://172.20.80.1:9000  http://192.168.128.1:9000  http://192.168.169.1:9000  http://192.168.56.1:9000  http://127.0.0.1:9000
	RootUser: minioadmin
	RootPass: minioadmin
	Console: http://192.168.137.217:9001 http://172.20.80.1:9001 http://192.168.128.1:9001 http://192.168.169.1:9001 http://192.168.56.1:9001 http://127.0.0.1:9001
	RootUser: minioadmin
	RootPass: minioadmin

	Command-line: https://min.io/docs/minio/linux/reference/minio-mc.html#quickstart
	   $ mc.exe alias set myminio http://192.168.137.217:9000 minioadmin minioadmin

	Documentation: https://min.io/docs/minio/linux/index.html
	Warning: The standard parity is set to 0. This can lead to data loss.
```

## Run docker with `minio`:
```bash
# set env for minio data
MINIO_DIR="/tmp/$USER/minio-data"

# create and mount this directory
mkdir -pv "$MINIO_DIR"
docker run -p 9000:9000 -p 9001:9001 -v "$MINIO_DIR":/data --name minio -d quay.io/minio/minio server /data --console-address ":9001"
```

Run `docker logs minio` and get auth data and endpoint API:
```bash
Formatting 1st pool, 1 set(s), 1 drives per set.
WARNING: Host local has more than 0 drives of set. A host failure will result in data becoming unavailable.
WARNING: Detected default credentials 'minioadmin:minioadmin', we recommend that you change these values with 'MINIO_ROOT_USER' and 'MINIO_ROOT_PASSWORD' environment variables
MinIO Object Storage Server
Copyright: 2015-2023 MinIO, Inc.
License: GNU AGPLv3 <https://www.gnu.org/licenses/agpl-3.0.html>
Version: RELEASE.2023-03-13T19-46-17Z (go1.19.7 linux/amd64)

Status:         1 Online, 0 Offline. 
API: http://172.17.0.3:9000  http://127.0.0.1:9000 
Console: http://172.17.0.3:9001 http://127.0.0.1:9001 

Documentation: https://min.io/docs/minio/linux/index.html
Warning: The standard parity is set to 0. This can lead to data loss.
```

## Run on linux with minica:
```bash
# get minio bin
curl -LO https://dl.min.io/server/minio/release/linux-amd64/minio
chmod u+x minio
# clone repo for gen certs
git clone https://github.com/jsha/minica

# install golang
# https://go.dev/doc/install

# build minica
cd minica
go build
sudo install ./minica /usr/local/bin
mkdir ./data
# ~~minica -domains '*.minio2.home'~~
minica -domains '*.minio2.home,minio2.home'
sudo cp _.minio2.home/cert.pem ${HOME}/.minio/certs/public.crt
sudo cp _.minio2.home/key.pem ${HOME}/.minio/certs/private.key

# open ports
firewall-cmd --get-services
# firewall-cmd --permanent --add-service=ssh
# firewall-cmd --permanent --add-port=4444/tcp
# firewall-cmd --permanent --remove-service=ssh

cat << EOF | sudo tee --append /etc/firewalld/services/minio.xml
<?xml version="1.0" encoding="utf-8"?>
<service>
  <short>Minio</short>
  <description>Console port is 9001 and api port is 9000</description>
  <port protocol="tcp" port="9000"/>
  <port protocol="tcp" port="9001"/>
</service>
EOF
firewall-cmd --permanent --add-service=minio
sudo firewall-cmd --reload
firewall-cmd --permanent --list-all

# run minio
export MINIO_SERVER_URL=https://api.minio2.home
export MINIO_BROWSER_REDIRECT_URL=https://console.minio2.home
sudo ./minio server --address :9000 --console-address :9001 \
	--certs-dir ${HOME}/.minio/certs/ ./data/

# don't forget to open 443/tcp port!
sudo ./minio server --console-address :443 --certs-dir ${HOME}/.minio/certs/ ./data/
```

After [installing our certificate](https://gist.github.com/tty8747/5762f8be50b080cd73d3772261d15817) `minica.pem` check minio by following the link: https://console.minio2.home:9001/login

> [Addition info about installing a certificate](https://thomas-leister.de/en/how-to-import-ca-root-certificate/)

> ```bash
> $ dig console.minio2.home. | grep 'ANSWER SECTION' -A2
> ;; ANSWER SECTION:
> console.minio2.home.	7059	IN	CNAME	minio2.home.
> minio2.home.		7059	IN	A	10.125.22.4
> $ dig api.minio2.home. | grep 'ANSWER SECTION' -A2
> ;; ANSWER SECTION:
> api.minio2.home.	6855	IN	CNAME	minio2.home.
> minio2.home.		6855	IN	A	10.125.22.4
> ```

## Work with API:

Now we can use `http://localhost:9001/` to create api keys and use `minio API` on the port `9000`:

> Several useful points for java:
> - [java-sdk](https://min.io/docs/minio/linux/developers/minio-drivers.html#java-sdk)
> - [quick start](https://min.io/docs/minio/linux/developers/java/minio-java.html)
> - [example app](https://min.io/docs/minio/linux/developers/java/minio-java.html#quick-start-example-file-uploader)
> - [API](https://github.com/minio/minio-java/blob/master/docs/API.md)
> - [Examples of method calls](https://github.com/minio/minio-java/tree/release/examples)
> - [My example](https://gitlab.com/java9901610/javaMinio) working on java with `minio api`.
> - [Binary files](https://dl.min.io/server/minio/release/)
> - [Also about certificates](https://github.com/minio/minio/blob/master/docs/tls/README.md), and [also](https://min.io/docs/minio/linux/operations/network-encryption.html)  
> - Centos 7.9: `WARNING: Detected Linux kernel version older than 4.0.0 release, there are some known potential performance problems with this kernel version. MinIO recommends a minimum of 4.x.x linux kernel version for best performance`
