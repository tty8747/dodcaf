---
author: "Sergey Chulanov"
title: "Traefik simple using"
date: 2023-03-22T09:11:19+03:00
description: >-
  Traefik is as a loadbalancer
tags:
  - traefik
  - loadbalancer
categories:
  - linux
  - devops
  - guide
series:
  - guide
draft: false
---

### Quick links

| Centos 7.9

Ok, let's start with linux. I'm using a bin file, so I want to check using traefik's options in yaml files. Get a binary and run it:
```bash
curl -LO https://github.com/traefik/traefik/releases/download/v2.9.8/traefik_v2.9.8_linux_amd64.tar.gz
tar -zxf traefik_v2.9.8_linux_amd64.tar.gz traefik
./traefik version
	Version:      2.9.8
	Codename:     banon
	Go version:   go1.19.6
	Built:        2023-02-15T15:23:25Z
	OS/Arch:      linux/amd64
```

> Traefik [releases](https://github.com/traefik/traefik/releases)

Disable selinux:
```bash
getenforce
	Enforcing
sudo setenforce 0
grep SELINUX= /etc/sysconfig/selinux | grep -v "^#"
SELINUX=disabled
```

Install `nginx` and `apache`. They will be services behind `traefik`.
```bash
sudo yum install epel-release -y

# Nginx
sudo yum install nginx -y
# change nginx port to 8080
grep "server" -A3 -m1 /etc/nginx/nginx.conf
    server {
        listen       8080;
        listen       [::]:8080;
    ...
    }
sudo systemctl enable --now nginx
curl -D - "http://localhost:8080"

# Apache
sudo yum install httpd -y
# change nginx port to 8080
grep Listen /etc/httpd/conf/httpd.conf | grep -v "^#"
Listen 8181
sudo systemctl enable --now httpd
curl -D - "http://localhost:8181"
```

Traefik configs  
Make directories for traefik:
```bash
mkdir ./ssl ./custom
```

Put the traefik.yml file:
```bash
cat <<EOF > traefik.yml
entrypoints:
  web:
    address: ":80"
    http:
      redirections:
        entryPoint:
          to: websecure
          scheme: https
          permanent: false
  websecure:
    address: ":443"

log:
  level: DEBUG

providers:
  file:
    directory: ./custom
    watch: true

certificatesResolvers:
  letsEncrypt:
    acme:
      email: myemail@awesome.com
      storage: ./ssl/acme.json
      # comment the line 'caServer:' if everything is ok, this line for letsencrypt staging, test aims
      # caServer: "https://acme-staging-v02.api.letsencrypt.org/directory"
      httpChallenge:
        entryPoint: web
EOF
```

Create redirect to our services:
```bash
cat <<EOF > custom/my-nginx-ssl.yml
http:
  routers:
    my-nginx-ssl:
      entryPoints:
        - websecure
      service: nginx-service
      rule: Host(\`nginx.aaaj.site\`)
      tls:
        certResolver: letsEncrypt
  services:
    nginx-service:
      loadBalancer:
        servers:
          - url: http://localhost:8080/
        passHostHeader: true
EOF
```
```bash
cat <<EOF > custom/my-apache.yml
http:
  routers:
    my-apache-ssl:
      entryPoints:
        - websecure
      service: apache-service
      rule: Host(\`apache.aaaj.site\`)
      tls:
        certResolver: letsEncrypt
  services:
    apache-service:
      loadBalancer:
        servers:
          - url: http://localhost:8181/
        passHostHeader: true
EOF
```

```bash
tree
	.
	├── custom
	│             ├── my-apache.yml
	│             └── my-nginx-ssl.yml
	├── ssl
	├── traefik
	└── traefik.yml
```
Create `A` and `CNAME` records for our traefik. For example, my ip address is `213.158.x.x`:
```
traefik     A           213.158.x.x
apache      CNAME       traefik.aaaj.site
nginx       CNAME       traefik.aaaj.site
```

Open ports (firewalld):
```
sudo firewall-cmd --get-services
# firewall-cmd --permanent --add-service=ssh
# firewall-cmd --permanent --add-port=4444/tcp
# firewall-cmd --permanent --remove-service=ssh

cat << EOF | sudo tee --append /etc/firewalld/services/traefik.xml
<?xml version="1.0" encoding="utf-8"?>
<service>
  <short>Traefik</short>
  <description>Traefik ports 80 and 443</description>
  <port protocol="tcp" port="80"/>
  <port protocol="tcp" port="443"/>
</service>
EOF

sudo firewall-cmd --permanent --add-service=traefik
sudo firewall-cmd --reload
sudo firewall-cmd --permanent --list-all
```

Run `traefik` and check:
```bash
sudo ./traefik
curl -D - "http://nginx.aaaj.site"
curl -D - "https://nginx.aaaj.site" -k
curl -D - "http://apache.aaaj.site"
curl -D - "https://apache.aaaj.site" -k
```
