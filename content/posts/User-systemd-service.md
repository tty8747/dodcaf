---
author: "Sergey Chulanov"
title: "User systemd service"
date: 2025-01-07T15:08:39+03:00
description: >-
  Systemd unit from user
tags:
  - systemd
  - journalctl
categories:
  - linux
series:
  - guide
draft: false
---

| Ubuntu 22.04

All of users' systemd have to place in directory ~/.config/systemd/user/

> If you want start user systemd unit when user login, run:
> ```bash
> systemctl --user enable <service>
> ```

List of available types of systemd units:
```bash
systemctl -t help
Available unit types:
service
mount
swap
socket
target
device
automount
timer
path
slice
scope
```

Create user systemd file to make ssh connection:

> Name "hetzner" was already created in `~/.ssh/config`:
> ```bash
> ...
> host hetzner
> Hostname 159.69.152.138
> Port 22
> user goto
> IdentityFile /home/goto/.ssh/id_rsa
> ...
```bash
cat <<EOF > ~/.config/systemd/user/ssh-hetznder-sockd5.service
[Unit]
Description=Setup a secure tunnel to %I
After=network.target

[Service]
Environment="TARGET=hetzner"
# EnvironmentFile=/etc/default/secure-tunnel@%i
ExecStart=/usr/bin/ssh -NT -o ServerAliveInterval=60 -o ExitOnForwardFailure=yes -D 1080 $TARGET

# Restart every >2 seconds to avoid StartLimitInterval failure
RestartSec=5
Restart=always
EOF
```

And reload systemd:
```bash
systemctl --user daemon-reload
```

Enable and start systemd service:
```bash
systemctl --user enable --now ssh-hetznder-sockd5
```

Check that service alive:
```bash
$ systemctl --user status ssh-hetznder-sockd5
● ssh-hetznder-sockd5.service - Setup a secure tunnel to
     Loaded: loaded (/home/goto/.config/systemd/user/ssh-hetznder-sockd5.service; enabled; vendor preset: enabled)
     Active: active (running) since Tue 2025-01-07 15:28:57 MSK; 5s ago
   Main PID: 643630 (ssh)
      Tasks: 1 (limit: 18149)
     Memory: 1.8M
        CPU: 54ms
     CGroup: /user.slice/user-1000.slice/user@1000.service/app.slice/ssh-hetznder-sockd5.service
             └─643630 /usr/bin/ssh -NT -o ServerAliveInterval=60 -o ExitOnForwardFailure=yes -D 1080 hetzner
```
