---
author: "Sergey Chulanov"
title: "Obtain keycloak token"
date: 2024-02-17T22:03:49+03:00
description: >-
  Obtain keycloak token
tags:
  - keycloak
categories:
  - linux
  - devops
series:
  - guide
draft: false
---

Prepare `info.json` with data of your app:
```json
{
  "host": "keycloak.your.domain.com",
  "realm": "your-realm-name",
  "username": "admin",
  "password": "admin123",
  "clientid": "name-of-your-client",
  "client_secret": "client_secret"
}
```

Prepare `script.sh`:
```bash
#!/usr/bin/env bash

set -eou pipefail

HOST=$(cat $1 | jq -r .host)
REALM=$(cat $1 | jq -r .realm)
USERNAME=$(cat $1 | jq -r .username)
PASSWORD=$(cat $1 | jq -r .password)
CLIENTID=$(cat $1 | jq -r .clientid)
CLIENTSECRET=$(cat $1 | jq -r .client_secret)

curl -s -X POST \
    https://$HOST/realms/$REALM/protocol/openid-connect/token \
    -H 'Content-Type: application/x-www-form-urlencoded' \
    -d username=$USERNAME \
    -d password=$PASSWORD \
    -d grant_type=password \
    -d client_id=$CLIENTID \
    -d client_secret=$CLIENTSECRET
```

And run:
```bash
./script.sh info.json
{"access_token":"eyJhbGciOiJSUzI1NiIsInR5cCIgO...BKESR8ybP9u7dsW8aMSEU21Fd8_ZcKA","token_type":"Bearer","not-before-policy":0,"session_state":"b71c0226-64b8-4f39-bcf3-a8813ff6c9c2","scope":"profile email roles"}
```

Check output in [jwt.io](https://jwt.io/)

> If you got `{"error":"RESTEASY003210: Could not find resource for full path:` remove `auth` from the path: `https://$HOST/auth/realms/$REALM/protocol/openid-connect/token` --> `https://$HOST/realms/$REALM/protocol/openid-connect/token` [for 17.0+ version of Keycloak](https://stackoverflow.com/questions/70577004/keycloak-could-not-find-resource-for-full-path).

