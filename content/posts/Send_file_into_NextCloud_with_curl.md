---
author: "Sergey Chulanov"
title: "Put file in a folder on NextCloud"
date: 2023-08-10T15:23:31+03:00
description: >-
  Put file in a folder on NextCloud
tags:
  - curl
  - nextcloud
categories:
  - linux
  - devops
series:
  - guide
draft: false
---

- Go to NextCloud, open Settings -> Security -> fill "App name" -> click "Create new app password", so we got login and pass.
- Create folder in your NextCloud.
- Fill varables with your own values:
  ```bash
    USER=96EEDA25-C3DA-XXXX-XXXX-XXXXXXXXXXXX
    PASS=3XabR-rygAb-XXXXX-XXXXX-XXXXX
    NEXTCLOUD_FOLDER=gitlab-backups
    PATH_TO_BACKUP=gitlab-backups
    BACKUP_NAME=1691665722_2023_08_10_14.9.2_gitlab_backup.tar
    NEXT_CLOUD_URL=https://your.cloud.tld
- Run the script:
  ```bash
    curl -k -u "$USER:$PASS" -T "$PATH_TO_BACKUP/$BACKUP_NAME" "$NEXT_CLOUD_URL/remote.php/webdav/$NEXTCLOUD_FOLDER/$BACKUP_NAME"
  ```

