---
author: "Sergey Chulanov"
title: "Git squash"
date: 2023-10-06T17:10:20+03:00
description: >-
  Git squash
tags:
  - git
categories:
  - linux
  - devops
series:
  - guide
draft: false
---

### Quick links

| Ubuntu 22.04

Git squash

> If it's needed, can set the default editor:
> `git config --global core.editor "vim --nofork"`

Сделаем ребейз нашей ветки на основную ветку, в данном случае это develop.
Before starting, it's needed to rebase our branch `feature/fea-37` to `develop` branch, for example.

Let's look at- the commit to merge:
```bash
git cherry -v develop
	+ 428db8b30e4732b179bfc7d4663c6860214ea343 fea-37
	+ b41e7856dd5715e3dbff8fd62a6e4a49e46e96e5 fea-37 1
	+ 06e55320a2f6682277a9702e6a796ac84eab849c fea-37 Improved ansible scripts
	+ d8380d0e994a16c79211a0ec0007676d429d077f fea-37 Changed db URI
	+ 90bad559fa3c2d9d1e6afb9b5816609212e4b931 fea-37 Returned template.env
	+ 1006b0cde85165f19148b3e7a7cb72ca8bf524d9 fea-37 Removed unnecessary host
	+ 3d1d95d9146d3b0bae94016ebc45f4c1252338cb fea-37 Moved and improved ansible Dockerfile
	+ 64b7c1cb7a1f10d27a5611b5a96d407235c38e15 fea-37 Added gitlab-ci from app2
	+ 76b04f223e87fbf9eb39c0cd7f3b64994e64b72f fea-37 Added delivery for test
	+ 0c2ca7465a4f9977fbc4df83734e11b5e0da7f08 fea-37
	+ 0b0f6c1787211d61e72d8e5f307580dc79269a8d fea-37 Added dump app1
```

Count it:
```bash
$ git cherry -v develop | wc -l
11
```

So, do `rebase` on 11 commits:
```bash
git rebase -i HEAD~11
```

And replace pick into squash, except the first line:
```bash
pick 428db8b30 fea-37
pick b41e7856d fea-37 1
pick 06e55320a fea-37 Improved ansible scripts
pick d8380d0e9 fea-37 Changed db URI
pick 90bad559f fea-37 Returned template.env
pick 1006b0cde fea-37 Removed unnecessary host
pick 3d1d95d91 fea-37 Moved and improved ansible Dockerfile
pick 64b7c1cb7 fea-37 Added gitlab-ci from app2
pick 76b04f223 fea-37 Added delivery for test
pick 0c2ca7465 fea-37
pick 0b0f6c178 fea-37 Added dump app1
```

It will look like that:
```bash
pick 428db8b30 fea-37
squash b41e7856d fea-37 1
squash 06e55320a fea-37 Improved ansible scripts
squash d8380d0e9 fea-37 Changed db URI
squash 90bad559f fea-37 Returned template.env
squash 1006b0cde fea-37 Removed unnecessary host
squash 3d1d95d91 fea-37 Moved and improved ansible Dockerfile
squash 64b7c1cb7 fea-37 Added gitlab-ci from app2
squash 76b04f223 fea-37 Added delivery for test
squash 0c2ca7465 fea-37
squash 0b0f6c178 fea-37 Added dump app1
```

After saving, git will offer to put on the commit-message.
Now can replace remote git branch:
```bash
git push --set-upstream origin feature/fea-37 --force
# or
git push --force
```
