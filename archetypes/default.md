---
author: "Sergey Chulanov"
title: "{{ replace .Name "-" " " }}"
date: {{ .Date }}
description: >-
  some description
tags:
  - sometag
categories:
  - linux
  - devops
  - ops
  - databases
  - guide
series:
  - guide
draft: false
---

### Quick links

| Ubuntu 22.04 | [link1](#link1) | [link-2](#awesome-link-1) | [link 3](#awesome-link-2)

# Link1
## Awesome link 1
### Awesome link 2
